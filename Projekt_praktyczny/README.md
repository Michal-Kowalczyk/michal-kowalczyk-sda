# Przykładowa aplikacja z wykorzystaniem:
- Spring MVC
- Spring Security
- Thymeleaf
- MongoDb


Aplikacja korzysta z Lomboka, wymaga zainstalowania pluginu w IntelliJ

"Biuro podrózy"
Aplikacja ma pozwalalać przeglądać oferty wycieczek i wyszukiwać w nich te nejbardziej interesujące użytkownika. Możliwość
wyszukania będzie się odbywać po takich parametrach jak te dostępne na stronie tui.pl. Dodatkowo uzytkownik będzie miał
możliwość rejestracji/zalogowania i kupienia wycieczki. Aplikacja będzie również posiadać panel administracyjny gdzie
pracownik będzie mógł usuwać, dodać, modyfikować wycieczki.

Projekt przygotowywany wspólnie z Damianem Zielińskim.

Projekt jest w trakcie realizacji.