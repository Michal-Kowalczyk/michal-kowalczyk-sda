function switchMenu() {
    var menu = document.getElementById("navigation");
    if(menu.className != "") {
        menu.className = "";
    }
    else {
        menu.className = "active";
    }
}

function inputChange(element) {
    var inputVal = element.value;
    if(!parseInt(inputVal)) {
        element.className = "invalid"
    } else {
        element.className = "";
    }
}

function silnia() {
    var inputElem = document.getElementById("num");
    var val = parseInt(inputElem.value);
    var result;
    if(!isNaN(val)) {
        result = count(val);
    } else {
        result = "nieprawidłowa wartość";
    }
    var resultElem = document.getElementById("result");
    resultElem.innerHTML = result;
}

function count(a) {
    var currentVal = 1;
    var currentStep = 1;
    do {
        currentVal *= currentStep;
        currentStep++;
    } while(currentStep <= a);
    return currentVal;
}


function tagTest() {
    var divElements = document.getElementsByTagName("div");
    for (var i = 0; i < divElements.length; i++) {
        divElements[i].innerHTML = "div znaleziony!";
        divElements[i].style.margin = "4rem";
    }
}

function classTest() {
    var elems = document.getElementsByClassName("test-class-one");
    for (var i = 0; i < elems.length; i++) {
        elems[i].className += " black";
    }
}

function idTest() {
    var element = document.getElementById("testThree");
    element.innerHTML = null;
    var newElement = document.createElement("div");
    newElement.className = "insider";
    element.appendChild(newElement);
}

function bonusFunc(elem) {
    elem.value = "Jeszcze raz!";
    elem.className += "extra-large";
    var newClickEventHandler = function() {
        console.log("magic!");
        elem.value = "WINCYJ!";
        elem.onclick = function () {
            window.alert("NIESTETY CZAS NA NAUKĘ :(");
            window.location = "http://www.w3schools.com/js/";
        };
    };
    elem.onclick = newClickEventHandler;
}

function resetExample() {
    location.reload();
}


//var a = 7;
//var currentVal = 1;
//var currentStep = 0;
//do {
//    currentVal *= ++currentStep;
//}
//while (currentStep < a);
//console.log(currentVal);