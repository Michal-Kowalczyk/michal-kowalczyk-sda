package pl.bkoziak.sda.jdbc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.bkoziak.sda.jdbc.support.JdbcTestHelper;
import pl.bkoziak.sda.jdbc.support.JdbcTestSupport;

import java.sql.*;

import static pl.bkoziak.sda.jdbc.support.JdbcTestHelper.CREATE_TABLE_CUSTOMERS;

/**
 * Created by bkoziak on 11.08.2016.
 */
public class SelectiveQueriesTest extends JdbcTestSupport {

    @Before
    public void prepareTestData() {
        JdbcTestHelper.insertTestData(6);
    }


    /**
     * Wykorzystując wiedzę z poprzednich zadań pobierz liczbę (count) wpisów w tabeli CUSTOMERS.
     * Zarówno tabela, jak również inicjalne dane są automatycznie dodane.
     */
    @Test
    public void testCounting() throws SQLException {
        JdbcTestHelper.printTable("CUSTOMERS");
        Long total = null;
        String customerTableCreateSql = CREATE_TABLE_CUSTOMERS;
        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;

        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) FROM CUSTOMERS");
        if (resultSet.next()) {
            total = resultSet.getLong(1);
        }

        Assert.assertEquals(Long.valueOf(6), total);
    }

    /**
     * Wyszukaj w tabeli CUSTOMERS rekord, który dla kolumny NAME posiada wartość zawartą w zmiennej name.
     * Dla pierwszego wyszukanego rekordu pobierz wartość kolumny AGE i przypisz do zmiennej age
     * Zarówno tabela, jak i dane są tworzone automatycznie
     * Dla chętnych do wykorzystania klasa PreparedStatement pobierana na podstawie connection.prepareStatement(sql)
     */
    @Test
    public void getAddressByNameTest() throws SQLException {
        JdbcTestHelper.printTable("CUSTOMERS");
        String name = "Ramesh";
        int age = -1;

        String sql = "SELECT * FROM CUSTOMERS WHERE NAME = '" + name + "'";

        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;

        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        if (resultSet.next()) {
            age = resultSet.getInt("AGE");
        }

        Assert.assertEquals(32, age);

        age = -1;

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM CUSTOMERS WHERE NAME = ?");
        preparedStatement.setString(1, name);
        if (preparedStatement.execute()) {
            ResultSet rs = preparedStatement.getResultSet();
            if (rs.next()) {
                age = rs.getInt("AGE");
            }
        }

        Assert.assertEquals(32, age);


    }
}
