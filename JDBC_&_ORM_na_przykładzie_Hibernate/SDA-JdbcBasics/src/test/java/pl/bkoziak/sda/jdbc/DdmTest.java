package pl.bkoziak.sda.jdbc;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import pl.bkoziak.sda.jdbc.support.JdbcTestHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static pl.bkoziak.sda.jdbc.support.JdbcTestHelper.CREATE_TABLE_CUSTOMERS;

/**
 * Created by bkoziak on 11.08.2016.
 */
public class DdmTest {

    @AfterClass
    public static void clean() {
        JdbcTestHelper.destroyTestDB();
    }

    /**
     * Na podstawie zmiennej customerTableCreateSql oraz wiedzy zdobytej w ConnectionCaptureTest utwórz
     * tabelę w bazie danych.
     */
    @Test
    public void customerTableCreateTest() throws SQLException {
        String customerTableCreateSql = CREATE_TABLE_CUSTOMERS;
        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;

        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        connection.createStatement().executeUpdate(customerTableCreateSql);

        Assert.assertTrue(JdbcTestHelper.tableExists("CUSTOMERS"));
    }
}
