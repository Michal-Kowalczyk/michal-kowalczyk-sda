package pl.bkoziak.sda.jdbc.support;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Created by bkoziak on 11.08.2016.
 */
public abstract class JdbcTestSupport {
    @BeforeClass
    public static void beforeClass() {
        JdbcTestHelper.createTestDB();
    }

    @AfterClass
    public static void afterClass() {
        JdbcTestHelper.destroyTestDB();
    }

    @After
    public void after(){
        JdbcTestHelper.truncateTestDB();
    }
}
