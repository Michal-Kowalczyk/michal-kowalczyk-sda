package pl.bkoziak.sda.jdbc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.bkoziak.sda.jdbc.support.JdbcTestHelper;
import pl.bkoziak.sda.jdbc.support.JdbcTestSupport;

import java.sql.*;
import java.util.logging.Logger;

/**
 * Created by bkoziak on 12.08.2016.
 */
public class UpdatingQueriesTest extends JdbcTestSupport {
    private static final Logger log = Logger.getLogger(UpdatingQueriesTest.class.getName());

    @Before
    public void prepareTestData() {
        JdbcTestHelper.insertTestData(12);
    }

    /**
     * Wykorzystując wiedze z poprzednich ćwiczeń zaktualizuj rekordy, których wartość kolumny ADRES równy jest wartości
     * zmiennej address, ustawiając w kolumnie SALARY wartość na wskazaną w zmiennej newSalary
     */
    @Test
    public void updateSalariesTest() throws SQLException {
        JdbcTestHelper.printTable("CUSTOMERS");
        int newSalary = 6000;
        String address = "MP";

        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;

        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        PreparedStatement ps = connection.prepareStatement("UPDATE CUSTOMERS SET SALARY = ? WHERE ADDRESS = ?");
        ps.setLong(1, newSalary);
        ps.setString(2, address);

        ps.executeUpdate();

        JdbcTestHelper.printTable("CUSTOMERS");

        Assert.assertEquals(6, JdbcTestHelper.countWithWhereClause("CUSTOMERS", "SALARY > 5000"));
        Assert.assertEquals(6, JdbcTestHelper.countWithWhereClause("CUSTOMERS", "SALARY <= 5000"));

    }

    /**
     * Wykorzystując wiedze z poprzednich ćwiczeń usuń rekordy, których wartość kolumny SALARY jest mniejszy lub równy
     * wartości określonej w zmiennej salary
     */
    @Test
    public void deletePeopleFrom() throws SQLException {
        JdbcTestHelper.printTable("CUSTOMERS");
        long salary = 2000;

        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;

        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        PreparedStatement ps = connection.prepareStatement("DELETE FROM CUSTOMERS WHERE SALARY <= ?");
        ps.setLong(1, salary);
        ps.executeUpdate();

        JdbcTestHelper.printTable("CUSTOMERS");

        Assert.assertEquals(0, JdbcTestHelper.countWithWhereClause("CUSTOMERS", "SALARY <= 2000"));
        Assert.assertEquals(6, JdbcTestHelper.countWithWhereClause("CUSTOMERS", "SALARY > 2000"));
    }
}
