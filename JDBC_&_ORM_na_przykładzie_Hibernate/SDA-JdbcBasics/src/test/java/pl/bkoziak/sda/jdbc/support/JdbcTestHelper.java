package pl.bkoziak.sda.jdbc.support;

import java.sql.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bkoziak on 11.08.2016.
 */
public abstract class JdbcTestHelper {

    private static final Logger log = Logger.getLogger(JdbcTestHelper.class.getName());


    public static final String CREATE_TABLE_CUSTOMERS = "CREATE TABLE CUSTOMERS(" +
            "ID BIGINT AUTO_INCREMENT," +
            "NAME VARCHAR (20) NOT NULL," +
            "AGE INT NOT NULL," +
            "ADDRESS CHAR (25)," +
            "SALARY DECIMAL(18, 2)," +
            "PRIMARY KEY (ID)" +
            ");";
    private static final String DROP_TABLE_CUSTOMERS = "DROP TABLE CUSTOMERS;";
    private static final String TRUNCATE_TABLE_CUSTOMERS = "TRUNCATE TABLE CUSTOMERS;";

    public static final List<String> INSERTS_TABLE_CUSTOMERS = Arrays.asList(
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('Ramesh', 32, 'Ahmedabad', 2000.00 );",
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('Khilan', 25, 'Delhi', 1500.00 );",
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('kaushik', 23, 'Kota', 2000.00 );",
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('Chaitali', 25, 'Mumbai', 6500.00 );",
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('Hardik', 27, 'Bhopal', 8500.00 );",
            "INSERT INTO CUSTOMERS (NAME,AGE,ADDRESS,SALARY) VALUES ('Komal', 22, 'MP', 4500.00 );"
    );


    public static final String CONNECTION_URL = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
    public static final String DB_USER = "sa";


    public static void createTestDB() {
        withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(CREATE_TABLE_CUSTOMERS);
                log.info(() -> "Table CUSTOMERS created");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static void destroyTestDB() {
        withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(DROP_TABLE_CUSTOMERS);
                log.info(() -> "Table CUSTOMERS dropped");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        });
    }

    public static void truncateTestDB() {
        withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(TRUNCATE_TABLE_CUSTOMERS);
                log.info(() -> "Table CUSTOMERS truncated");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static void insertTestData(int count) {
        if (count <= 0) {
            throw new IllegalArgumentException("Count must be positive integer");
        }
        withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();

                int predefinedInsertsSize = INSERTS_TABLE_CUSTOMERS.size();
                IntStream.range(0, count)
                        .mapToObj(idx -> INSERTS_TABLE_CUSTOMERS.get(idx % predefinedInsertsSize))
                        .forEach(insert -> {
                            try {
                                statement.executeUpdate(insert);
                                log.info(() -> "Insert record to database done");
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        });

                log.info(() -> "All inserts to database done");


            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        });
    }

    public static void printTable(String tableName) {
        withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName);
                int columnCount = resultSet.getMetaData().getColumnCount();

                List<String> columnNames = new LinkedList<String>();
                for (int i = 1; i <= columnCount; ++i) {
                    columnNames.add(resultSet.getMetaData().getColumnName(i));
                }

                String header = columnNames.stream().collect(Collectors.joining("\t"));


                StringBuilder tableStringBuilder = new StringBuilder("== Table " + tableName + " data start").append("\n");
                tableStringBuilder.append(header).append("\n");
                while (resultSet.next()) {
                    String row = columnNames.stream()
                            .map(column -> {
                                try {
                                    return resultSet.getObject(column).toString();
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            })
                            .collect(Collectors.joining("\t"));
                    tableStringBuilder.append(row).append("\n");
                }
                tableStringBuilder.append("== Table " + tableName + " data end").append("\n");
                log.info(tableStringBuilder.toString());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static void withConnection(Consumer<Connection> handler) {
        try (Connection connection = DriverManager.getConnection(
                CONNECTION_URL,
                DB_USER,
                null)) {

            log.info(() -> "Connected to " + CONNECTION_URL);
            handler.accept(connection);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <RESULT> RESULT withConnection(Function<Connection, RESULT> handler) {
        try (Connection connection = DriverManager.getConnection(
                CONNECTION_URL,
                DB_USER,
                null)) {

            log.info(() -> "Connected to " + CONNECTION_URL);
            return handler.apply(connection);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean tableExists(String tableName) {
        return withConnection(connection -> {
            try {
                ResultSet tables = connection.getMetaData().getTables(null, null, tableName, null);
                return tables.next();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static long countWithWhereClause(String table, String whereClause) {
        return withConnection(connection -> {
            try {
                Statement statement = connection.createStatement();
                String query = new StringBuilder("SELECT COUNT(*) AS TOTAL FROM ")
                        .append(table)
                        .append(" WHERE ")
                        .append(whereClause)
                        .toString();
                ResultSet resultSet = statement.executeQuery(query);
                if (resultSet.next()) {
                    return resultSet.getLong("TOTAL");
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return -1l;
        });
    }
}
