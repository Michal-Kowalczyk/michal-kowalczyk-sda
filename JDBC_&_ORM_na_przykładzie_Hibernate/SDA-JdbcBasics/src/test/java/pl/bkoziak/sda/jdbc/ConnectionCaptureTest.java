package pl.bkoziak.sda.jdbc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.bkoziak.sda.jdbc.support.JdbcTestSupport;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by bkoziak on 11.08.2016.
 */
public class ConnectionCaptureTest {

    /**
     * Wykorzystaj klasę java.sql.DriverManager do pozyskania połączenia do bazy. Jako parametrów użyj
     * powyższych zmiennych
     */
    @Test
    public void loadDriverTest() throws SQLException {
        String jdbcUrl = "jdbc:h2:mem:jdbc;DB_CLOSE_DELAY=-1;MVCC=TRUE";
        String user = "sa";
        String password = null;
        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        Assert.assertNotNull(connection);
    }
}
