package pl.bkoziak.sda.hibernate.basics.course;

import javax.persistence.*;
import java.util.List;

/**
 * Created by RENT on 2016-08-13.
 */

@Entity
@Table(name = "STUDENTS_GROUP")

public class StudentsGroup {
    private Long id;
    private String alias;
    private List<Student> students;

    public StudentsGroup() {
    }

    @Id
    @GeneratedValue

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @ManyToMany
    @JoinTable(name = "STUDENTS_GROUP_STUDENT", joinColumns = @JoinColumn(name = "STUDENT_GROUP_ID"),
        inverseJoinColumns = @JoinColumn(name = "STUDENT_ID")
    )

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
