package pl.bkoziak.sda.hibernate.basics.course;

import javax.persistence.*;
import java.util.List;

/**
 * Created by RENT on 2016-08-13.
 */


@Entity

public class Student {
    private Long id;
    private String firstName;
    private String lastName;
    private String pesel;

    @ManyToMany(mappedBy = "students")
    public List<StudentsGroup> getStudentsGroups() {
        return studentsGroups;
    }

    public void setStudentsGroups(List<StudentsGroup> studentsGroups) {
        this.studentsGroups = studentsGroups;
    }

    private List<StudentsGroup> studentsGroups;

    public Student() {
    }

    @Id
    @GeneratedValue

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "PESEL", length = 11)
    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}