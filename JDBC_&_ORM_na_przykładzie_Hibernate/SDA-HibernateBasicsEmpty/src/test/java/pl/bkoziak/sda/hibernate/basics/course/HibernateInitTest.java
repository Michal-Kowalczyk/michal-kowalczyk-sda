package pl.bkoziak.sda.hibernate.basics.course;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by RENT on 2016-08-13.
 */
public class HibernateInitTest {

    @Test
    public void hibernateInitTest() {
        Configuration configuration = new Configuration().configure();

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session session = sessionFactory.openSession();
        Assert.assertNotNull(session);

        Transaction transaction = session.beginTransaction();

        Student student = new Student();
        student.setFirstName("Jan");
        student.setLastName("Kowalski");
        student.setPesel("12312312312");

        session.save(student);


        StudentsGroup studentsGroup = new StudentsGroup();
        studentsGroup.setAlias("Drużyna A");

        session.save(studentsGroup);

        transaction.commit();
        sessionFactory.close();
    }
}
