var Snake = function() {
	this.bodyParts = [];
	this.dlugosc = 0;
	this.moveStep = 1;
	
	function BodyPart(x,y){
		this.x = x;
		this.y = y;
	}

	this.zainicjuj = function() {
		this.bodyParts.push(new BodyPart(2,0));
		this.bodyParts.push(new BodyPart(1,0));
		this.bodyParts.push(new BodyPart(0,0));
	}

	this.move = function(kierunek, wys, szer) {

		var newHead;

		if(this.bodyParts[0].x >= szer-1 && kierunek == 'right') {
			newHead = new BodyPart(0, this.bodyParts[0].y);
		} else if (this.bodyParts[0].x <= 0 && kierunek == 'left') {
			newHead = new BodyPart(szer-1, this.bodyParts[0].y);
		} else if(this.bodyParts[0].y >= wys-1 && kierunek == 'down') {
            newHead = new BodyPart(this.bodyParts[0].x,0);
        } else if(this.bodyParts[0].y <= 0 && kierunek == 'up') {
            newHead = new BodyPart(this.bodyParts[0].x,wys-1);
        } else {
            newHead = this.getNewHead(kierunek);    
        }

		// if(this.bodyParts[0].x == szer - 1 || this.bodyParts[0].y == wys - 1) {
		// 	return false;
		// }

		// newHead = this.getNewHead(kierunek);

		for(var i = this.bodyParts.length-1; i>0; i--){
			this.bodyParts[i] = this.bodyParts[i-1];
		}
		this.bodyParts[0] = newHead;

		return true;
	}

	this.grow = function(kierunek) {
		var currentHead = this.bodyParts[0];
		var newHead;
	

		switch(kierunek){
			case 'right':
				newHead = new BodyPart(currentHead.x+this.moveStep, currentHead.y);
				break;
			case 'left':
				newHead = new BodyPart(currentHead.x-this.moveStep, currentHead.y);
				break;
			case 'up':
				newHead = new BodyPart(currentHead.x,currentHead.y-this.moveStep);
				break;
			case 'down':
				newHead = new BodyPart(currentHead.x,currentHead.y+this.moveStep);
				break;
		}

		this.bodyParts.unshift(newHead);
	}

	this.getNewHead = function(kierunek){
		var currentHead = this.bodyParts[0];
		switch(kierunek){
			case 'right':
				return new BodyPart(currentHead.x+this.moveStep, currentHead.y);
			case 'left':
				return new BodyPart(currentHead.x-this.moveStep, currentHead.y);

			case 'up':
				return new BodyPart(currentHead.x,currentHead.y-this.moveStep);
			case 'down':
				return new BodyPart(currentHead.x,currentHead.y+this.moveStep);
		}
	}

}



