var Plansza = function () {

	this.wysokosc = 0;
	this.szerokosc = 0;
	this.plansza = null;
	this.snake = null;
	this.time = null;
	this.kierunek = 'right';

	this.rysuj = function(wys,szer) {
		this.plansza = document.getElementById("plansza");
		this.wysokosc = wys;
		this.szerokosc = szer;

		this.time = setInterval(this.przerysuj.bind(this), 100);

		for(var i = 0; i < wys; i++) {
			for(var j = 0; j < szer; j++) {
				var el = document.createElement("div");
				el.className = "plytka";
				el.setAttribute("data-pos", j+", "+i);
				this.plansza.appendChild(el);
			}
		}
	}

	this.zmienKierunek = function(kierunek) {
		this.kierunek = kierunek;
	}

	this.przerysuj = function() {
		var plytki = document.querySelectorAll("div.plytka.snake");
		//plytki[0].className="plytka";
		
		for(var i = 0; i < plytki.length; i++) {
			plytki[i].className = "plytka";
		}

		if (this.snake.move(this.kierunek, this.wysokosc, this.szerokosc)) {
			this.drawSnake();
		}
	}


	this.collision = function(){
		this.locateApple();
		this.snake.grow(this.kierunek);
	}

	this.drawSnake = function(){

   	for(var i = 0; i <this.snake.bodyParts.length; ++i) {
   		var plytka = document.querySelectorAll("[data-pos='"+this.snake.bodyParts[i].x+", "+this.snake.bodyParts[i].y+"']");

   		if(i == 0) {
   			if(plytka[0].className == "plytka apple") {
   				this.collision();
   			}
   		}

        plytka[0].className = "plytka snake";
   	}


	    var query = "[data-pos='"+this.snake.bodyParts[0].x+", "+this.snake.bodyParts[0].y+"']";
		var plytki = document.querySelectorAll(query);

		for(var i = 0; i < plytki.length; i++) {
				plytki[i].className = "plytka snake";
		}
	}




	this.locateApple = function() {
		var x = 4, y = 5;

        var x = Math.floor(Math.random() * this.wysokosc);
        var y = Math.floor(Math.random() * this.szerokosc);

		var plytka = document.querySelectorAll("[data-pos='"+x+", "+y+"']")
		plytka[0].className = "plytka apple";
	}



}


