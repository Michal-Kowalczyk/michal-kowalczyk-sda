var myName = 'Maciej';

var dog1 = {
  myName: 'Azor'
};

var dog2 = {
  myName: 'Burek'
};

function logName() {
  console.log(this.myName);
}

logName();
logName.call(dog1);
logName.call(dog2);
