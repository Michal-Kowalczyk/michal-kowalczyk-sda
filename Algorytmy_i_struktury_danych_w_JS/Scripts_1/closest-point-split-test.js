var p0 = {x: 2, y: -4};
var p = [{x: 3, y: -4}, {x: 0, y: 0}, {x: -4, y: -9999}];

function calculateDistanceEst(p1, p2) {
  return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

function findClosestPoint(refPoint, points) {
  var i,
    closestPoint,
    closestDistance = Number.MAX_VALUE,
    distanceEst,
    point;

  for (i = 0; i < points.length; ++i) {
    point = points[i];
    distanceEst = calculateDistanceEst(refPoint, point);
    if (distanceEst < closestDistance) {
      closestPoint = point;
      closestDistance = distanceEst;
    }
  }

  return closestPoint;
}

var closest = findClosestPoint(p0, p);
if (closest.x === 3 && closest.y === -4) {
    console.log('pass');
} else {
    console.log('fail');
}
