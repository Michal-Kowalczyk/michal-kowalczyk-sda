var refPoint = {x: 0, y: 0};
var points = [
  {x: 0, y: 215},
  {x: 2, y: 66},
  {x: -15, y: 314},
  {x: -20, y: 0},
  {x: 22, y: -3},
  {x: -20, y: -20}
];

function calculateDistanceEst(p1, p2) {
  return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

function findClosestPoint(refPoint, points) {
  var i,
    closestPoint,
    closestDistance = Number.MAX_VALUE,
    distanceEst,
    point;

  for (i = 0; i < points.length; ++i) {
    point = points[i];
    distanceEst = calculateDistanceEst(refPoint, point);
    if (distanceEst < closestDistance) {
      closestPoint = point;
      closestDistance = distanceEst;
    }
  }

  return closestPoint;
}

console.log('Closest point:', findClosestPoint(refPoint, points));
