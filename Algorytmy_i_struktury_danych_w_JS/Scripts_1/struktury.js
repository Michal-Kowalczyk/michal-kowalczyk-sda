var points = [
  {x: 0, y: 215},
  {x: 2, y: 66},
  {x: -15, y: 314},
  {x: -20, y: 0},
  {x: 22, y: -3},
  {x: -20, y: -20}
];

var pO = {
  x: 0,
  y: 0
};

var dXPOP0 = pO.x - points[3].x;
var dYPOP0 = pO.y - points[3].y;
var dXPOP0Pow = dXPOP0 * dXPOP0;
var dYPOP0Pow = dYPOP0 * dYPOP0;
var distance = Math.sqrt(dXPOP0Pow + dYPOP0Pow);

console.log(distance);
