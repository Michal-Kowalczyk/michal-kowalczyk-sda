var refPoint = {x: 0, y: 0};
var points = [
  {x: 0, y: 215},
  {x: 2, y: 66},
  {x: -15, y: 314},
  {x: -20, y: 0},
  {x: 22, y: -3},
  {x: -20, y: -20}
];

function findClosestPoint(refPoint, points) {
  var i,
    closestPoint,
    closestDistance = Number.MAX_VALUE,
    distanceEst,
    point;

  for (i = 0; i < points.length; ++i) {
    point = points[i];
    // Calculate distance between points.
    distanceEst = (point.x - refPoint.x) * (point.x - refPoint.x) +
      (point.y - refPoint.y) * (point.y - refPoint.y);
    // If smaller, consider this point a temporary closest point.
    if (distanceEst < closestDistance) {
      closestPoint = point;
      closestDistance = distanceEst;
    }
  }

  return closestPoint;
}

console.log('Closest point:', findClosestPoint(refPoint, points));
