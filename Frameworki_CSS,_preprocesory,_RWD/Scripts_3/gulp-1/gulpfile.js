var gulp = require('gulp'),
  sass = require('gulp-sass'),
  coffee = require('gulp-coffee');

gulp.task('sass', function () {
  gulp.src('app/styles/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/styles'));
});

gulp.task('coffee', function () {
  gulp.src('app/scripts/**/*.coffee')
    .pipe(coffee())
    .pipe(gulp.dest('app/scripts'));
});

gulp.task('watch', function () {
  gulp.watch('app/scripts/**/*.coffee', ['coffee']);
  gulp.watch('app/styles/**/*.scss', ['sass']);
});

gulp.task('build', ['sass', 'coffee', 'watch']);
