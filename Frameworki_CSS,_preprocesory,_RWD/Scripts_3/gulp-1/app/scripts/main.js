(function() {
  var Animal, Dog,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Animal = (function() {
    function Animal() {
      this.a = 15;
    }

    Animal.prototype.walk = function() {};

    return Animal;

  })();

  Dog = (function(superClass) {
    extend(Dog, superClass);

    function Dog() {
      return Dog.__super__.constructor.apply(this, arguments);
    }

    Dog.prototype.walk = function() {
      return this.a = 20;
    };

    return Dog;

  })(Animal);

}).call(this);
