(function () {

  var canvas = document.createElement('canvas'),
    context = canvas.getContext('2d'),
    car1 = new Car();
    car2 = new Car();

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  document.body.appendChild(canvas);

  car2.startAccelerating();

  function raf() {
    requestAnimationFrame(raf);
    gameLoop();
    render();
  }

  function gameLoop() {
    car1.operate();
    car2.operate();
  }

  function render() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = 'red';
    context.fillRect(car1.position.x * 0.01, 100, 50, 20);
    context.fillStyle = 'blue';
    context.fillRect(car2.position.x * 0.01, 200, 50, 20);
  }

  function onKeyDown(event) {
    if (event.keyCode == 38) {
      // UP
      car1.startAccelerating();
    } else if (event.keyCode == 40) {
      // DOWN
      car1.break();
    }
  }

  function onKeyUp() {
    car1.stopAccelerating();
  }

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);
  raf();

}());
