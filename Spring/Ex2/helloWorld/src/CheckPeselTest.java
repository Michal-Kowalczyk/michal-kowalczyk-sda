import org.junit.Assert;
import org.junit.Test;

public class CheckPeselTest {

    @Test
    public void shouldCheckIsCorrectPesel() {
        //given
        CheckPesel checkPesel = new CheckPesel();
        String correctPesel = "74778887738";

        //when
        boolean isCorrect = checkPesel.verify(correctPesel);

        //then
        Assert.assertTrue("Should be correct",isCorrect);
    }

}
