import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class HelloWorld {

    public static void main(String... args) {
        System.out.println("start");
        ApplicationContext applicationContext =
                new FileSystemXmlApplicationContext("classpath:application-context.xml");
        A a =  applicationContext.getBean(A.class);
        a.doSomething(true);
    }
}
