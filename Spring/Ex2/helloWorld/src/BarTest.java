import org.junit.Assert;
import org.junit.Test;

public class BarTest {

    @Test
    public void shouldTellWhat() {
        //given
        Bar bar = new Bar();

        //when
        String result = bar.tellMe();

        //then
        Assert.assertTrue("Should return string result",
                "what?".equals(result));
        Assert.assertTrue("Should not be null", result != null);
        Assert.assertTrue("Should not return empty",
                !"".equals(result));
    }

}
