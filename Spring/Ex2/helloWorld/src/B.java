public class B implements IB {

    @Override
    public void foo() {
        System.out.println("run foo");
    }

    @Override
    public String getSecureMethod() {
        return "empty";
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
