public interface IB {

    void foo();

    String getSecureMethod();

    boolean isSecure();
}
