
public class SecureB implements IB {

    @Override
    public void foo() {
        System.out.println("secure");
    }

    @Override
    public String getSecureMethod() {
        throw new IllegalAccessError();
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
