import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application-context.xml")
public class ATest implements ApplicationContextAware {

    ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Test
    public void shouldDependencyInject() {
        //given
        A a = applicationContext.getBean(A.class);

        //when

        //then
        Assert.assertTrue("Was context injected",
                a.getIb() != null && a.getIbSecure() != null);
    }

    class SecureMock implements IB {

        @Override
        public void foo() {

        }

        @Override
        public String getSecureMethod() {
            return "RSA";
        }

        @Override
        public boolean isSecure() {
            return true;
        }

        @Test
        public void shouldFetchSecureMethod() {
            //given
            A a = applicationContext.getBean(A.class);
            //when
            a.setIbSecure(new SecureMock());
            String result = a.getSecureMethod();
            //then
            Assert.assertTrue("Should not be null", result != null);
        }
    }
}
