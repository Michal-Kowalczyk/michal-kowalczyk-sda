import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;

public class A implements BeanNameAware, BeanFactoryAware, ApplicationContextAware, InitializingBean, DisposableBean {

    private IB ib;

    private IB ibSecure;

    private int number;

    private List<String> stringList;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public void setSomeText(String someText) {
        this.someText = someText;
    }

    private String someText;

    public A() {
        System.out.println("A()");
    }

    public String getSecureMethod() {
        if(ibSecure.isSecure())
            return ibSecure.getSecureMethod();
        else return "-";
    }

    public void doSomething(boolean isSecure) {
        if(isSecure) {
            ibSecure.foo();
        }
        else {
            ib.foo();
        }
    }

    public void setIb(IB ib) {
        this.ib = ib;
    }

    public void setIbSecure(IB ibSecure) {
        this.ibSecure = ibSecure;
    }

    public IB getIb() {
        return ib;
    }

    public IB getIbSecure() {
        return ibSecure;
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("setBeanName() [" + s + "]");
    }

    @Override
    public void setBeanFactory (BeanFactory beanFactory) throws BeansException {
        System.out.println("setBeanFactory() [" + beanFactory.toString() + "]");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("setApplicationContext() [" + applicationContext.toString() + "]");
    }

    public void afterPropertiesSet() {
        System.out.println("afterPropertiesSet()");
    }

    public void init() {
        System.out.println("init");
    }

    public void destroyCustom() {
        System.out.println("destroyCustom");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy()");
    }
}
