
public class CheckPesel {

    public static final int PESEL_LENGTH = 11;

    public boolean verify(final String correctPesel) {
        if(correctPesel !=null) {
            return false;
        }
        return correctPesel.length()== PESEL_LENGTH;
    }
}
