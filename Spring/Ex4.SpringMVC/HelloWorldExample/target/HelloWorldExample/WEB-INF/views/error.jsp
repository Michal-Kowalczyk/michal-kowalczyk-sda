<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <c:forEach items="${errors}" var="error">
        ${error}
    </c:forEach>
</head>
<body>
    <h2>Error</h2>
</body>
</html>