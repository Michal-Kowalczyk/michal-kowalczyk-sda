package com.sda.bean;


public interface IProduct {
     Category getCategory();

    float getPrice();

    String getName();

    String getDescription();

    boolean isDiscout();

    float getDiscoutPrice();
}
