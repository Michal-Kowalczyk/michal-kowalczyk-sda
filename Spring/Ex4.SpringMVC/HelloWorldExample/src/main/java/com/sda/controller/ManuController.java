package com.sda.controller;

import com.sda.bean.Car;
import org.hibernate.validator.internal.engine.ValidatorFactoryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by RENT on 2016-09-01.
 */
@Controller
public class ManuController {

    @RequestMapping(value = "/createTrousers", method = RequestMethod.GET)
    public ModelAndView getTrousersForm() {
        return new ModelAndView("createTrousersForm").addObject("car", new Car());
    }

    @RequestMapping(value = "/createTrousers", method = RequestMethod.POST)
    public ModelAndView handleTrousersForm(@ModelAttribute Car car) {

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator =  validatorFactory.getValidator();
        Set<ConstraintViolation<Car>> validationErrors = validator.validate(car);
        if (validationErrors.isEmpty()) {
            return new ModelAndView("showCreatedCar").addObject("car", car);
        } else {
            return new ModelAndView("error").addObject("errors",validationErrors.
                    stream().map(ConstraintViolation::getMessage).collect(Collectors.toList()));
        }
    }
}
