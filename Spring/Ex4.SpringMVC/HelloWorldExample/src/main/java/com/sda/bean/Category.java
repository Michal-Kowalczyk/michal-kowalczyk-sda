package com.sda.bean;

public enum Category {

    BOOK,
    NEWSPAPER,
    ICE_CREAM,
    DRINK,
    RICE,
    TROUSERS,
    SHOES

    }
