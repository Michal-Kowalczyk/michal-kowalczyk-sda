package com.sda.bean;

public abstract class AbstractProduct implements  IProduct {

    protected Category category;

    protected float price;

    protected String name;

    protected String description;

    protected boolean isDiscout;

    protected float discoutPrice;

    public AbstractProduct() {

    }

    public AbstractProduct(final Category category, final float price, final String name) {
        this.category = category;
        this.name = name;

        if (price > 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price can't be below 0.");
        }
    }

    public Category getCategory(){

        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDiscout(boolean discout) {
        isDiscout = discout;
    }

    public void setDiscoutPrice(float discoutPrice) {
        this.discoutPrice = discoutPrice;
    }

    public float getPrice(){
        return price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isDiscout() {
        return isDiscout;
    }

    @Override
    public float getDiscoutPrice() {
        return discoutPrice;
    }

    @Override
    public String toString() {
        return name;
    }
}
