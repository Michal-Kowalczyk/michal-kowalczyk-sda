package com.sda;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by RENT on 2016-08-31.
 */

@Controller
public class HelloWorldController {

    @RequestMapping(value = "/employee/{name}", method = RequestMethod.GET)
    public ModelAndView sayHelloWorld(@PathVariable String name) {
        if ("Janusz".equals(name)) {
            return new ModelAndView("error");
        }
        return new ModelAndView("helloworld").addObject("name", name);
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView handleFormPage() {
        return new ModelAndView("register");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView handleFormSubmit(@ModelAttribute(name = "name") String imie, @ModelAttribute(name = "password") String haslo) {
        return new ModelAndView("thankyoupage").addObject("name", imie).addObject("password", haslo);
    }
}