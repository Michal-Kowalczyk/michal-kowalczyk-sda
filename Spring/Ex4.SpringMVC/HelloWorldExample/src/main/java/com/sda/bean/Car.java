package com.sda.bean;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by RENT on 2016-09-01.
 */
public class Car {

    @NotEmpty(message = "nie moze byc puste")
    private String nazwa;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public boolean isNowy() {
        return nowy;
    }

    public void setNowy(boolean nowy) {
        this.nowy = nowy;
    }

    @NotEmpty
    private String marka;

    private boolean nowy;
}
