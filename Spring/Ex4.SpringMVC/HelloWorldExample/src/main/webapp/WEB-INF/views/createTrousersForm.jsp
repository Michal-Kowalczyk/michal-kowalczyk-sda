<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<body>
    <h2>Formularz</h2>
    <form:form action="/HelloWorldExample/createTrousers" method="post" modelAttribute="car">

        <form:label path="nazwa" >Nazwa:</form:label>
        <form:input path="nazwa"/>

        <form:label path="marka" >Marka:</form:label>
        <form:input path="marka"/>

        <form:label path="nowy" >Czy samochód jest nowy?</form:label>
        <form:checkbox path="nowy"/>

        <input type="submit">
    </form:form>
</body>
</html>