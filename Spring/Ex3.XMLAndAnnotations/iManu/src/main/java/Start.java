package main.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class Start {

    public static void main(String... args) {
        ApplicationContext applicationContext =
                new FileSystemXmlApplicationContext("classpath:application-context.xml");
    }
}
