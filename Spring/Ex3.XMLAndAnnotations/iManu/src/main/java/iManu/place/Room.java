package main.java.iManu.place;

public class Room {
    public int number;

    public int floor;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
        System.out.println("Room " + number);
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
        System.out.println("Floor " + floor);
    }
}
