package main.java.iManu.place;

import java.util.LinkedList;
import java.util.List;

public class Building {
    List<Room> availRooms = new LinkedList<>();

    public static final int MAX_ROOM_ON_FLOR = 2;
    public int takenRoom = 1;
    public void init() {
        availRooms.add(new Room());
        availRooms.add(new Room());
        availRooms.add(new Room());
        availRooms.add(new Room());
        System.out.println("Init");
    }

    Room getAvailRoom() {
        Room room = availRooms.get(0);
        availRooms.remove(0);
        System.out.println("getRoom");
        return room;
    }
}
