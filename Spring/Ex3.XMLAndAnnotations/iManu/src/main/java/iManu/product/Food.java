package main.java.iManu.product;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;

public class Food extends AbstractProduct {
    public Food(Category category, float price, String name) {
        super(category, price, name);
    }
}
