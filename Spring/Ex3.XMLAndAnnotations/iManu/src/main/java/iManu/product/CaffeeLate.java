package main.java.iManu.product;

import main.java.iManu.Category;
import main.java.iManu.Size;

public class CaffeeLate extends Coffee {
    Size size;

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    public CaffeeLate(Category category, float price, String name) {
        super(category, price, name);

    }
}
