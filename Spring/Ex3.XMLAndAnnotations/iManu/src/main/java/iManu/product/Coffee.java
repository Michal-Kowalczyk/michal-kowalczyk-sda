package main.java.iManu.product;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;
import main.java.iManu.Size;

public class Coffee extends AbstractProduct {
    Size size;

    public void setSize(Size size) {
        this.size = size;
    }

    public Coffee(Category category, float price, String name) {

        super(category, price, name);
    }
}
