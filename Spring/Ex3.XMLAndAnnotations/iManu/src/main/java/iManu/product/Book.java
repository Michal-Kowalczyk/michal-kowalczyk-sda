package main.java.iManu.product;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component(value = "ksiazka")
public class Book extends AbstractProduct {
    @Value("${book.name}")
    private String bookName;

    @PostConstruct
    public void init() {
        System.out.println("Książka " + bookName);
        category = Category.BOOK;
        price = 10.0f;
        name= bookName;
    }
}
