package main.java.iManu.product;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component(value = "jeansy")
public class Trousers extends AbstractProduct {
    @Value("${jeans.name}")
    private String jeansName;

    @PostConstruct
    public void init() {
        System.out.println("Jeans " + jeansName);
        category = Category.TROUSERS;
        price = 150.0f;
        name= jeansName;
    }
}