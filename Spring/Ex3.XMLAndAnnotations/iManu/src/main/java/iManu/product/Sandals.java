package main.java.iManu.product;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component(value = "kuboty")
public class Sandals extends AbstractProduct {

    @Value("${sandals.name}")
    private String sandalName;

    @PostConstruct
    public void init() {
        System.out.println("sandals " + sandalName);
        category = Category.SHOES;
        price = 100.0f;
        name= sandalName;
    }
}
