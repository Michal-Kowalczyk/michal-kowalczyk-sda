package main.java.iManu.console;

import java.util.function.Supplier;

public class Key {

    public String keyInput;

    public String description;

    public Supplier<Boolean> action;

    public Key(String keyInput, String description, Supplier<Boolean> action) {
        this.keyInput = keyInput;
        this.description = description;
        this.action = action;
    }
}
