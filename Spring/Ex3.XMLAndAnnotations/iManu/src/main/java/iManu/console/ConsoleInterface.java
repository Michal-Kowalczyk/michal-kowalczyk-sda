package main.java.iManu.console;

import main.java.iManu.MultiStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

@Component
public class ConsoleInterface {

    private Scanner keyboard = new Scanner(System.in);

    private List<Key> availableKey = new LinkedList<>();

    @Autowired
    private MultiStore multiStore;

    @PostConstruct
    public void init() {
        availableKey.add(new Key("e","Kończy działanie aplikacji",this::onExit));
        availableKey.add(new Key("h","Pomoc",this::onHelp));
        availableKey.add(new Key("l","Wypisuje listę sklepów",this::onShowShops));
        availableKey.add(new Key("p","Pokazuje produkty w sklepach",this::onProductInShops));
    }

    private Boolean onProductInShops() {
        System.out.println("Podaj nr sklepu");
        String input = keyboard.next();
        System.out.println(multiStore.getShops().get(Integer.valueOf(input)).getProducts().toString());

        return true;
    }

    private Boolean onShowShops() {
            multiStore.showShops();
        return true;
    }

    private Boolean onHelp() {
        for (Key key :availableKey) {
            System.out.println(key.keyInput + "-" + key.description);
        }
        return true;
    }

    private Boolean onExit() {
        isDone = true;
        return true;
    }

    private boolean isDone;

    public void run() {
        System.out.println("Witamy w aplikacji");
        onHelp();

        while (!isDone) {
            System.out.println(">");
            String input = keyboard.next();
            for (Key key: availableKey) {
                if (key.keyInput.equals(input)) {
                    key.action.get();
                }
            }
        }
    }
}
