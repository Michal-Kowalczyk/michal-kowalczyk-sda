package main.java.iManu;

import main.java.iManu.place.Room;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractStore implements IStore {

    private List<IProduct> products;

    public void setProducts(List<IProduct> products) {

        this.products = products;
    }
    private boolean isOpen;

    private Room room;

    private String name;

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    @Override
    public List<IProduct> getProducts() {
        return products;
    }

    @Override
    public List<IProduct> getDiscoutProducts() {

        List<IProduct> discountProductsList = new LinkedList<>();

        for (IProduct product : products) {

            if (product.isDiscout()){
                discountProductsList.add(product);
            }
        }
        return discountProductsList;
    }

    @Override
    public List<IProduct> getProductsByCategory(Category category) {

        List<IProduct> productByCategory = new LinkedList<>();

        for (IProduct product : products) {

            if (product.getCategory().equals(category)) {
                productByCategory.add(product);
            }
        }
        return productByCategory;
    }

    @Override
    public List<Category> getCategories() {

        List<Category> categories = new LinkedList<>();

        for (IProduct product : products) {
            if (!categories.contains(product.getCategory())) {
                categories.add(product.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

