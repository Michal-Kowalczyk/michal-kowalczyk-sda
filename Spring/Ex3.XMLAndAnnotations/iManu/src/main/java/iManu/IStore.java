package main.java.iManu;

import main.java.iManu.place.Room;

import java.util.List;


public interface IStore {

    List<IProduct> getProducts();

    List<IProduct> getDiscoutProducts();

    List<IProduct> getProductsByCategory(Category category);

    List<Category> getCategories();

    boolean isOpen();

    Room getRoom();

    void setRoom(Room room);

    String getName();
}
