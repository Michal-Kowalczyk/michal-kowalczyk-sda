package main.java.iManu;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MultiStore {

    private List<IStore> shops;

    private int openHour;

    private int closeHour;

    public List<IStore> getShops() {
        return shops;
    }
    @Autowired
    public void setShops(List<IStore> shops) {
        this.shops = shops;
    }

    public int getOpenHour() {
        return openHour;
    }

    public void setOpenHour(int openHour) {
        this.openHour = openHour;
    }

    public int getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(int closeHour) {
        this.closeHour = closeHour;
    }
    public void showShops(){

        for (int i =1; i < shops.size(); i++) {
            System.out.println("nr: " + i + " " + shops.get(i));
        }
    }

}
