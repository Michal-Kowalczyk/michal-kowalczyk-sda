package main.java.iManu;


public interface IProduct {
     Category getCategory();

    float getPrice();

    String getName();

    String getDescription();

    boolean isDiscout();

    float getDiscoutPrice();
}
