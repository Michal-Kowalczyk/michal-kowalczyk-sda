import main.java.iManu.MultiStore;
import main.java.iManu.console.ConsoleInterface;
import main.java.iManu.store.ClothesStore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class Start {

    public static void main(String... args) {
        ApplicationContext applicationContext =
                new FileSystemXmlApplicationContext("classpath:application-context.xml");


        ConsoleInterface consoleInterface = applicationContext.getBean(ConsoleInterface.class);
        consoleInterface.run();
    }
}
