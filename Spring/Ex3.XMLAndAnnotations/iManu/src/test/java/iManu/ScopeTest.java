package test.java.iManu;

import main.java.iManu.MultiStore;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("clsspath:application-context.xml")


public class ScopeTest implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Test
    public void testScope() {

        // given
       MultiStore m1;
       MultiStore m2;

        // when
        m1 = applicationContext.getBean(MultiStore.class);
        m1.setCloseHour(16);

        m2 = applicationContext.getBean(MultiStore.class);
        m2.setCloseHour(18);

        //then
        Assert.assertTrue("does exist", m1 != null);
        Assert.assertTrue("shoud close at 16", m1.getCloseHour() == 16);

    }
}
