package test.java.iManu;

import main.java.iManu.AbstractProduct;
import main.java.iManu.Category;
import org.junit.Assert;
import org.junit.Test;

public class AbstractProductTest {

    class TestProduct extends AbstractProduct {

        public TestProduct(Category category, float price, String name) {
            super(category, price, name);
        }
    }

    @Test
    public void shouldHasCategory() {
        //given
        TestProduct abstractProduct = new TestProduct(Category.BOOK, 10.0f,"tst");

        //when
        Category category = abstractProduct.getCategory();

        //then
        Assert.assertTrue("should not be null", category != null);
        Assert.assertTrue("should be", category.equals(Category.BOOK));
    }

    @Test
    public void shouldHavePrice() {
        //given
        TestProduct abstractProduct = new TestProduct(Category.BOOK, 10.0f,"tst");

        //when
        float price = abstractProduct.getPrice();

        //then
        Assert.assertTrue("Price should be positive.", price > 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCheckPrice() {
        new TestProduct(Category.BOOK, -10.0f,"tst");
    }
}
