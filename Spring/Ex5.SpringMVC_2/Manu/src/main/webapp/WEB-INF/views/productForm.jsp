<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

<h2>Nowy produkt</h2>
<form:form action="/Manu/createProduct" method="post" modelAttribute="product" >

    <form:label path="name" >Nazwa:</form:label>
    <form:input path="name"/>
    <br/>
    <form:label path="price" >Cena:</form:label>
    <form:input path="price"/>
    <br/>
    <form:label path="productCategory" >Kategoria:</form:label>
    <form:select path="productCategory">
        <form:options items="${categories}" />
    </form:select>
    <br/>
    <form:label path="shop.id" >Sklep:</form:label>
    <form:select path="shop.id">
        <form:options items="${stores}" itemLabel="name" itemValue="id" />
    </form:select>
    <br/>
    <input type="submit">

</form:form>
</body>
</html>
