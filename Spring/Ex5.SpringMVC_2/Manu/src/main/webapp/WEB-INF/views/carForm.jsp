<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

<c:if test="${isCarSaved == true}" >
    <div style="border: solid 1px">
        Nowy samochód:<br/>
        Nazwa: ${car.name}<br/>
        Typ: ${car.type}<br/>
    </div>
</c:if>

<h2>Nowy samochód</h2>
<form:form action="/Manu/createCar" method="post" modelAttribute="car" >

    <form:label path="name" >Nazwa:</form:label>
    <form:input path="name"/>
    <br/>
    <form:label path="type" >Typ:</form:label>
    <form:select path="type">
        <form:option value="VAN">Van</form:option>
        <form:option value="SEDAN">Sedan</form:option>
        <form:option value="PICKUP">Pickup</form:option>
    </form:select>
    <br/>
    <%--<form:select path="asd">--%>
        <%--<form:options items="${enumy}" />--%>
    <%--</form:select>--%>

    <input type="submit">

</form:form>
</body>
</html>
