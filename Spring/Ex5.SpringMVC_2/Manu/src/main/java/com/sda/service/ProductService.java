package com.sda.service;

import com.sda.beans.Product;
import com.sda.beans.Store;

import java.util.List;

public interface ProductService {
    boolean saveProduct(Product product);
    List<Product> getProductsByStore(Store store);
    void deleteProduct(Long productId);
}