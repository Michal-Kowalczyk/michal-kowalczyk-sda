package com.sda.controller;

import com.sda.beans.Product;
import com.sda.enums.ProductCategory;
import com.sda.service.ProductService;
import com.sda.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    StoreService storeService;

    @RequestMapping(value = "/createProduct", method = RequestMethod.GET)
    public ModelAndView getProductForm() {
        return new ModelAndView("productForm")
                .addObject("product", new Product())
                .addObject("categories", ProductCategory.values())
                .addObject("stores", storeService.getStores());
    }

    @RequestMapping(value = "/createProduct", method = RequestMethod.POST)
    public ModelAndView handleProductForm(@Valid @ModelAttribute Product product, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
            return new ModelAndView("error")
                    .addObject("errors", errors);
        }

        productService.saveProduct(product);
        return new ModelAndView("productForm")
                .addObject("product", product)
                .addObject("categories", ProductCategory.values())
                .addObject("stores", storeService.getStores());
    }

    @RequestMapping(value = "/deleteProduct/{productId}", method = RequestMethod.GET)
    public String handleDeleteProduct(@PathVariable Long productId, @RequestParam String storeId) {

        productService.deleteProduct(productId);

        return "redirect:/getStore/" + storeId;
    }
}
