package com.sda.beans;

import com.sda.enums.ProductCategory;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ProductCategory productCategory;

    @Range(min = 1)
    private float price;

    @NotEmpty
    private String name;
    private String description;
    private boolean isDiscout;
    private float discoutPrice;

    @ManyToOne
    private Store shop;

    public Store getShop() {
        return shop;
    }

    public void setShop(Store shop) {
        this.shop = shop;
    }


    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDiscout() {
        return isDiscout;
    }

    public void setDiscout(boolean discout) {
        isDiscout = discout;
    }

    public float getDiscoutPrice() {
        return discoutPrice;
    }

    public void setDiscoutPrice(float discoutPrice) {
        this.discoutPrice = discoutPrice;
    }

    @Override
    public String toString() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
