package com.sda.beans;

public enum CarType {
    VAN, PICKUP, SEDAN
}
