package com.sda.service.impl;

import com.sda.beans.Store;
import com.sda.repositories.StoreRepository;
import com.sda.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultStoreService implements StoreService {

    @Autowired
    StoreRepository storeRepository;

    @Override
    public List<Store> getStores() {
        return storeRepository.findAll();
    }

    @Override
    public boolean saveStore(Store store) {
//        storeRepository.findOne(1L);
//        storeRepository.findAll();
//        storeRepository.delete(1L);
        return storeRepository.save(store) != null;

    }

    @Override
    public Store getStoreById(Long id) {
        return storeRepository.findOne(id);
    }
}
