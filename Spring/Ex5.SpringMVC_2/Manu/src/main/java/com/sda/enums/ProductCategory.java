package com.sda.enums;

public enum ProductCategory {

    BOOK,
    NEWSPAPER,
    ICE_CREAM,
    DRINK,
    RICE,
    TROUSERS,
    SHOES

    }
