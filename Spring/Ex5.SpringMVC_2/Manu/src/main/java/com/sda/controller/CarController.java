package com.sda.controller;

import com.sda.beans.Car;
import com.sda.beans.CarType;
import com.sda.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CarController {

    @Autowired
    CarService carService;

    @RequestMapping(value = "/createCar", method = RequestMethod.GET)
    public ModelAndView getCarForm() {
        return new ModelAndView("carForm").addObject("car", new Car());
    }

    @RequestMapping(value = "/createCar", method = RequestMethod.POST)
    public ModelAndView handleCarForm(@ModelAttribute Car car) {

        carService.saveCar(car);
        return new ModelAndView("carForm")
                .addObject("car", car)
                .addObject("isCarSaved", true);
    }

}
