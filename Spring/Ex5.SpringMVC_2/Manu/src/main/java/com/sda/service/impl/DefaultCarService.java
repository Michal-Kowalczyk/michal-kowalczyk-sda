package com.sda.service.impl;

import com.sda.beans.Car;
import com.sda.repositories.CarRepository;
import com.sda.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultCarService implements CarService {

    @Autowired
    CarRepository carRepository;

    @Override
    public boolean saveCar(Car car) {
        Car savedCar = carRepository.save(car);
        return savedCar != null;
    }
}
