package com.sda.service;

import com.sda.beans.Store;

import java.util.List;

public interface StoreService {
    List<Store> getStores();
    boolean saveStore(Store store);
    Store getStoreById(Long id);
}
