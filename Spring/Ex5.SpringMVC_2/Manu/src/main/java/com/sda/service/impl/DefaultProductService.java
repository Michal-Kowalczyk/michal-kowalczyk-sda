package com.sda.service.impl;

import com.sda.beans.Product;
import com.sda.beans.Store;
import com.sda.repositories.ProductRepository;
import com.sda.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultProductService implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public boolean saveProduct(Product product) {
        return productRepository.save(product) != null;
    }

    @Override
    public List<Product> getProductsByStore(Store store) {
        return productRepository.findByShop(store);
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.delete(productId);
    }
}
