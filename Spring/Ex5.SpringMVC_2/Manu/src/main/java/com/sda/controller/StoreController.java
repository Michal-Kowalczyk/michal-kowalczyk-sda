package com.sda.controller;

import com.sda.beans.Car;
import com.sda.beans.Product;
import com.sda.beans.Store;
import com.sda.enums.StoreType;
import com.sda.service.CarService;
import com.sda.service.ProductService;
import com.sda.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class StoreController {

    @Autowired
    StoreService storeService;

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/stores", method = RequestMethod.GET)
    public ModelAndView getStores() {
        return new ModelAndView("stores")
                .addObject("stores", storeService.getStores());
    }

    @RequestMapping(value = "/createStore", method = RequestMethod.GET)
    public ModelAndView getStoreForm() {
        return new ModelAndView("storeForm")
                .addObject("store", new Store())
                .addObject("types", StoreType.values());
    }

    @RequestMapping(value = "/createStore", method = RequestMethod.POST)
    public ModelAndView handleStoreForm(@ModelAttribute Store store) {

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Store>> validationErrors = validator.validate(store);

        if(validationErrors.isEmpty()) {
            storeService.saveStore(store);
            return new ModelAndView("storeForm")
                    .addObject("store", store)
                    .addObject("types", StoreType.values())
                    .addObject("isStoreSaved", true);
        } else {
            return new ModelAndView("storeForm")
                    .addObject("store", store)
                    .addObject("errors", validationErrors.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList()));
        }
    }


    @RequestMapping(value = "/getStore/{storeId}", method = RequestMethod.GET)
    public ModelAndView getStoreDetails(@PathVariable Long storeId) {

        Store store = storeService.getStoreById(storeId);
        List<Product> products = productService.getProductsByStore(store);

        return new ModelAndView("storeDetails")
                .addObject("store", store)
                .addObject("products", products);
    }

}
