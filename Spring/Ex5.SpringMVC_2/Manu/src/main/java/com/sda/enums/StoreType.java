package com.sda.enums;

public enum StoreType {

    BOOKSHOP,
    CLOTHES_STORE,
    FOOD_STORE,
    RESTAURANT,

    }
