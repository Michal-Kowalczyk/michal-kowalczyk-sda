package com.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {

    @RequestMapping(value = "/hello/{name}", method = RequestMethod.GET)
    public ModelAndView sayHelloWorld(@PathVariable String name) {
        return new ModelAndView("helloworld").addObject("name", name);
    }
}
