<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

    <div>
        <h2>Sklepy:</h2>
        <ul>
            <c:forEach items="${stores}" var="store" varStatus="index">
                <li>${index.index + 1}. <a href="/Manu/getStore/${store.id}">${store.name}</a></li>
            </c:forEach>
        </ul>

    </div>

</body>
</html>
