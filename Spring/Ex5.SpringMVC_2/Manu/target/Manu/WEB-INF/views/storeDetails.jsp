<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

<div>
    <h2>Nazwa sklepu: ${store.name}</h2>
    <h2>Opis sklepu: ${store.description}</h2>
</div>
<div>
    <h2>Produkty:</h2>
    <ul>
        <c:forEach items="${products}" var="product" varStatus="index">
            <li>${index.index + 1}. ${product.name} <a href="/Manu/deleteProduct/${product.id}?storeId=${store.id}">Usuń</a></li>
        </c:forEach>
    </ul>
</div>

</body>
</html>
