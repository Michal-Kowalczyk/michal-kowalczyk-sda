<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <h1>Error</h1>
    <ul>
        <c:forEach items="${errors}" var="error" >
            <li>${error}</li>
        </c:forEach>
    </ul>
</body>
</html>
