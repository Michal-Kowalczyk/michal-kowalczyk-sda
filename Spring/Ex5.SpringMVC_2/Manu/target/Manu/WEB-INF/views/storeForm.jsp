<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

<c:if test="${errors != null}">
    <div>
        <h2>Errors:</h2>
        <c:forEach items="${errors}" var="error">
            ${error}
        </c:forEach>
    </div>
</c:if>

<h2>Nowy sklep</h2>
<form:form action="/Manu/createStore" method="post" modelAttribute="store" >

    <form:label path="name" >Nazwa:</form:label>
    <form:input path="name"/>
    <br/>
    <form:label path="description" >Opis:</form:label>
    <form:input path="description"/>
    <br/>
    <form:label path="storeType" >Typ:</form:label>
    <form:select path="storeType">
        <form:options items="${types}" />
    </form:select>
    <br/>
    <%--<form:select path="asd">--%>
        <%--<form:options items="${enumy}" />--%>
    <%--</form:select>--%>

    <input type="submit">

</form:form>
</body>
</html>
