import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by RENT on 2016-08-25.
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Witam");
        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:application-context.xml");
        A a = applicationContext.getBean(A.class);
        a.doSomething(true);
    }

}
