import org.junit.Assert;
import org.junit.Test;

/**
 * Created by RENT on 2016-08-25.
 */
public class CheckPeselTest {
    @Test
    public void shouldCheckIsCorrectPesel() {
        //given
        CheckPesel checkPesel = new CheckPesel();
        String correctPesel = "12345678911";

        //when
        boolean isCorrect = checkPesel.verify(correctPesel);

        //then
        Assert.assertTrue("Should be correct", isCorrect);
    }
}
