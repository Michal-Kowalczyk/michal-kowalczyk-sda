import org.junit.Assert;
import org.junit.Test;

/**
 * Created by RENT on 2016-08-25.
 */
public class BarTest {

    @Test
    public void shouldTellWhat() {
        //given- przygotowanie danych wejsciowych
        Bar bar = new Bar();

        //when - wywolanie logiki
        String result = bar.tellMe();

        //then - przetestowanie wyniku (assert)
        Assert.assertTrue("Should return string result", "what?".equals(result));
        Assert.assertTrue("Should not be null", result!=null);
        Assert.assertTrue("Should not return empty", !"".equals(result));

//        String check = "what?";
//        if (check.equals(bar.tellMe())) {
//            System.out.println("jest ok");
        }
    }
//}
