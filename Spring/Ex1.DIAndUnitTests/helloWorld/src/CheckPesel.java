/**
 * Created by RENT on 2016-08-25.
 */
public class CheckPesel {

    public static final int PESEL_LENGTH = 11;

    public boolean verify(final String correctPesel) {
        return correctPesel.length()== PESEL_LENGTH;
    }
}
