/**
 * Created by RENT on 2016-08-25.
 */
public class A {

    private IB ib;
    //pole w klasie - zmienna o nazwie ib

    private IB ibSecure;

    public IB getIb() {
        return ib;
    }

    public IB getIbSecure() {
        return ibSecure;
    }
//    public A(final IB ib) {
//        this.ib = ib;
//    }

    public void doSomething(boolean isSecure) {
        if(isSecure) {
            ibSecure.foo();
        }
        else {
            ib.foo();
        }
    }


    public void setIb(IB ib) {
        this.ib = ib;
    }

    public void setIbSecure(IB ibSecure) {
        this.ibSecure = ibSecure;
    }
}
