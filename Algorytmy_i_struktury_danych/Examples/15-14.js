// Napisz funkcję, w której dla zadanego łańcucha znaków, wszystkie znaki - takie same jak pierwsza litera ciągu znaków zostaną zamienione na znak '_', wyjątkiem jednak jest pierwszy znak. Dla przykładu:
// Wejście: oksymoron
// Wyjście: oksym_r_n

var str = 'oksymoron'
var firstLetter = str[0];
console.log(firstLetter + str.split(firstLetter).join("_").substring(1, str. length));


// alternatywne rozwiazanie:

var temp_str = firstLetter;
for(var i = 1; i < str.length; i++) {
	if(str[i] != firstLetter) {
		temp_str += str[i];
		} else {
			temp_str += "_";
		}
}

console.log(temp_str);