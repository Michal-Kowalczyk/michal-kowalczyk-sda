// Dane jest osiedle bloków. Każdy blok zawiera 2 klatki, po 2 piętra (+ parter), na każdym piętrze znajdują się dwa mieszkania. Napisz skrypt, który wyświetli adres każdego mieszkania tak, aby można było zaadresować kopertę. Bloki znajdują się tylko pod numerami nieparzystymi - pierwszy blok znajduje się pod numerem 1. Funkcja powinna przyjmować nazwę ulicy oraz ostatni numer, który występuje na danej ulicy (może być parzysty lub nieparzysty) oraz na wyjściu drukować poszczególne adresy.


function printAddresses(street, number) {
	if(number % 2 == 0) number--;
	for(var i = 1; i <= number; i = i + 2) {
		for(var j = 1; j <= 12; j++) {
			console.log("ul. " + street + " " + i + "/" + ( (j < 7) ? 'A' : 'B' ) + "/" + j);
		}
	}
}

printAddresses("Algorytmiczna", 4);