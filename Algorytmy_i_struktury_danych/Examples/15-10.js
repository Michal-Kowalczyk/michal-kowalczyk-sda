// Z Krakowa do Zakopanego są 132 kilometry. Napisz algorytm, który będzie podawał czas w jaki należy przebyć tę drogę przy różnych prędkościach (zakładamy, że pojazd porusza się całą drogę prędkością jednostajną).

function time (speed) {
    var s = 132;
    var t = (s/ speed) * 60;
    return t;
}
 
console.log(time(70));

