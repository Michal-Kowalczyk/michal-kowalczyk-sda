// Napisz algorytm liczący ile potrzeba elementów (bloczków) dla piramidy o poziomie N (1 poziom - 1 bloczek, 2 poziom - 2 bloczki itd.).

function piramid(n) {
	var sum = 0;
	//zakladamy, ze na poczatku mamy 0 bloczkow;
	for(var i = 1; i <= n; ++i) {
		sum += 1;
	}
	return sum;
} 

console.log(piramid(5));