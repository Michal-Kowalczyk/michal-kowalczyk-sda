// Napisz funkcję, która przyjmuje jako argumenty: g - godziny, m - minuty, s - sekundy, a następnie zwraca podany czas w sekundach.


function inSeconds(g,m,s) {
	if (g >= 0 && m >= 0 && s >= 0) 
		return g*3600 + m*60 + s;
	return null;
}

console.log(inSeconds(1,5,70));
