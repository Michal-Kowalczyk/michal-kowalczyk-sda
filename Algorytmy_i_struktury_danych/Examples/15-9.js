// Kasia ulokowała w banku pewną ilość złotych na okres jednego roku. Oprocentowanie roczne w tym banku wynosi 19,4 %. Napisz algorytm, który będzie obliczał ilość pieniędzy na koncie po jednym roku dla dowolnej sumy pieniędzy. Zmodyfikuj program tak, aby obliczał kwotę dla wczytanej liczby lat.

function cash (value, years) {
	var percent = 1.194;
	var sum = value;
	for (var i = 1; i <= years; i++) {
		// suma = suma * percent;
		sum*=percent;
	}
	return sum;
}

console.log(cash(100,2), cash(100,4));
