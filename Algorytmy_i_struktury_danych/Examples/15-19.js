// Napisz funkcję, która jako argument przyjmuje dodatnią liczbę całkowitą większą od zera. Dla podanego zakresu wydrukuj kolejne wartości pomijając te, które są podzielne przez 3 lub przez 4.


function checkDivisionBy4(num) {
	var numArr = num.toString().split("");
	var str = "";
	if(numArr.length > 1) {
		str += numArr[numArr.length - 2];
	}
	str += numArr[numArr.length - 1];
	if(parseInt(str) % 4 === 0) { 
		return true;
	} else {
		return false;
	}
}

		// alternatywny zapis
		// return (parseInt(str) % 4 === 0) ? true:false;



function checkDivisionBy3(num) {
	var numArr = num.toString().split(""),
		sum = 0;
	for (var i = 0; i < numArr.length; i++) {
		sum += parseInt(numArr[i]);
	}
	return (sum % 3 == 0) ? true:false;
}


function checkNumberFormat(num) {
	return (typeof num == 'number' && (num|0 === num) && num > 0);
}


function printNumbers(num) {
	if(checkNumberFormat(num)) {
		for(var i = 0; i < num; i++) {
			if(!checkDivisionBy4(i) && !checkDivisionBy3(i)) {
				console.log(i);
			}
		}
	}
}

printNumbers(15);