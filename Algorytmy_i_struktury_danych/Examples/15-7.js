// Napisz funkcję, która dla zadanej liczby zwróci sumę kwadratów poszczególnych liczb od 1 do zadanej liczby. Przyjmij i zbadaj czy użytkownik przekazał liczbę w przedziale <0, 10>

function numberSquare(n) {
	var sum = 0;
	if (n >= 0 && n <= 10) {
		for (var i = 0; i <= n; i++) {
			sum += i*i;
		}
	}
	return sum;
}

// numberSquare(4);

console.log(numberSquare(4));