// 13. Mamy dwie tablice liczb całkowitych. Należy:
// 1. wyświetlić te liczby, które występują w obydwu tablicach
// 2. wyświetlić liczby z obu tablic, które się nie powtarzają

var arr = [2, 3, 4, 5, 6, 7, 8, 9, 10]
var arr2 = [1, 3, 6, 8, 10, 11, 12, 13, 14]

function inArray(arr, elem) {
	for (var i = 0; i < arr.length; i++) {
		if(arr[i] === elem) return true; 
	}
	return false;
}

function checkArrays(arr, arr2) {
	for (var i = 0; i < arr.length; i++) {
		if(inArray(arr2, arr[i])) console.log(arr[i]);
	}
	for (var i = 0; i < arr2.length; i++) {
		if(!inArray(arr, arr2[i])) console.log(arr2[i]);
	}
}

checkArrays(arr, arr2);