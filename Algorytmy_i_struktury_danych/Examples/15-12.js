// Na farmie występuje n królików. Nieparzyste z nich mają niestety tylko jedno ucho, parzyste są w komplecie. Dla wczytanego n wypisz ile uszu znajduje się na farmie.

function countEars(rabbits) {
	return (rabbits % 2 == 0) ? (rabbits/2*3) : (((rabbits -1)/2*3)+1);
}

var krooliki = prompt("Podaj liczbe krolikow, dla ktorych chcesz obliczyc ilosc uszu")

console.log(countEars(krooliki));