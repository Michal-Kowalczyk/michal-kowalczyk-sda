// Gra - zgadywanie liczby naturalnej. System losuje liczbę z zadanego przedziału, następnie użytkownik proszony jest o podanie kolejnych liczb. Jeżeli liczba jest taka sama jak wygenerowana, należy wyświetlić odpowiedni komunikat oraz zakończyć działanie skryptu.
// Przyjmij, że na początku użytkownik:
// a) podaje liczbę maksymalnych prób zgadnięć (po przekroczeniu prób, wyświetlana jest wylosowana liczba oraz komunikat o niepowodzeniu)
// b) nie podaje maksymalnej liczby zgadnięć - skrypt działa dopóki użytkownik nie zgadnie liczby, przy wpisaniu -1 skrypt powinien wyświetlić ilość podjętych prób, wygenerowaną liczbę oraz zakończyć działanie


// a)
function guessNumber() {
	var lives = parseInt(prompt("Podaj ilosc prob:")),
		num = Math.random() * 10 | 0;
	for (var i = 0; i < lives; i++) {
		if (parseInt(prompt("Podaj liczbe: ")) == num) {
			alert("BRAWO! Wygrales. Liczba " + num + "zostala odgadnieta w " + 1 + "probach");
			return;
		}
	}
	console.log("PRZEGRANA! Wylosowana liczba to " + num + ". Miales " + lives + " prob na zgadniecie!");
}
guessNumber();


// b)
function guessNumber() {
	var num = Math.random() * 10 | 0, userNum = null, i = 1;
	while(userNum != -1) {
		userNum = parseInt(prompt("Podaj liczbe: "));
		if(userNum == num) {
			alert("Brawo! Zgadles! Za " + i + " razem");
			return;
		}
		i++;
	}
	alert("Przegrana!");
}
guessNumber();