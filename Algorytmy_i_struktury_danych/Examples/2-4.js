// Napisz klasę emulującą działanie szyfru Cezara. Klasa powinna zawierać metodę szyfrującą oraz deszyfrującą (oraz ewentualnie metodę). Przyjmij, że szyfr powinien działać dla małych liter, bez polskich znaków.

var vector = 5, str = 'pawel';

function caesarCipher(str, vector) {
	var alphabet = "", cipher = '';
	for(var i = 97; i <= 122; i++) {
		alphabet += String.fromCharCode(i);
	}
	for(var i = 0; i < str.length; i++) {
		arg = alphabet.indexOf(str[i]) + vector;
		if(arg >= alphabet.length) arg -= alphabet.length;
		cipher += alphabet[arg];
	}
	return cipher;
}

console.log(caesarCipher(str, vector));