// Napisz funkcję, sprawdzającą czy przekazany argument jest palindromem w zależności od przekazanego typu danych. Dla przykładu, funkcja zwraca True dla:
// - 'oko'
// - 'kajak'
// - 5225
// - 'no i lata batalion', 'kobyla ma maly bok' (należy uwzględnić pomijanie spacji przy sprawdzaniu palindromu)
// - [1, 2, 3, 2, 1]

function isPalindrome(arg) {
	if(typeof arg == 'number') {
		arg = arg.toString();
	} else if(typeof arg == 'object') {
		for(var i = 0; i < Math.floor(arg.length/2); i++) {
			if(arg[i] != arg[arg.length-i-1]) return false;
		}
		return true;
	}
	return arg == arg.split("").reverse().join("");
}

var str = "kajak";
var num = 5225;
var arr = [1, 2, 3, 2, 1];

console.log(isPalindrome(arr));