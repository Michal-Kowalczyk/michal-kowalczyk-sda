// Napisz funkcję liczącą NWD wykorzystując algorytm Euklidesa.


// REKURENCYJNIE

function NWD (a,b) {
	if (b != 0) {
		return NWD(b, a%b);
	}
	return a;
}

console.log(NWD(15,10), NWD(0,10), NWD(15,0), NWD(0,0));




//ITERACYJNIE

// function NWD(a,b) {
//     while(a!=b) {
//        while(a>b) {
//            a-=b; 
//            // (a=a-b)
//        }
//        while(b>a) {
//            b-=a; 
//            // (b=b-a)
//            }
//        }
//     return a;
// }

// console.log = NWD(12,24); 

