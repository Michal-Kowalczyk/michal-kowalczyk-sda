// Użytkownik podaje dwie liczby całkowite a, b. Algorytm ma za zadanie wypisać wszystkie parzyste liczby w kolejności rosnącej, a następnie wszystkie liczby nieparzyste w kolejności malejącej z przedziału <a;b>. Niech a, b –liczby całkowite z zakresu 0-255. Np. dla danych wejściowych a=3, b=8, otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3.

function printNumbers(a, b) {
  var arr = [];
  // badz arr = [];
  for (i = a; i <= b; i++) {
    if (i%2 == 0) arr.push(i);
  }
  for (i = b; i >= a; i--) {
    if (i%2!= 0) arr.push(i);
  }
  return arr;
}

console.log(printNumbers(3,8));




// alternatywa


(function printAnotherNumbers(a, b) {
  var arr_e = [];
  var arr_o = [];
  for(i=a; i<=b; i++) {
    if(i%2 == 0) arr_e.push(i);
    else arr_o.push(i);
  }
  console.log(arr_e + ',' + arr_o.reverse());

}) (3,8);