package com.sda.example;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TableService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TableService.class);

    /**
     * Rozmiar bloku na ktorym pracuje algorytm.
     */
    private static final int BLOCK_SIZE = 8;

    /**
     * ZNak ktorym zostaje wypelniony string zeby mozna bylo
     * go podzielic na bloki.
     */
    private static String EMPTY = " ";

    /**
     * Drukuje tablice na konsoli.
     *
     * @param input tablica do wypisania.
     */
    public <T> void show(final T[][] input) {
        for (int i = 0; i < input.length; i++) {
            T[] tmpArray = input[i];
            for (int j = 0; j < tmpArray.length; j++) {
                System.out.print(tmpArray[j] + "|");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }

    /**
     * Metoda odejmuje komorki tablic i zwraca ich sume w trzeciej
     * tablicy
     *
     * @param input1 - 1 tablica wejsciowa
     * @param input2 - 2 tablica wejsciowa
     * @return tablica wynikowa
     */
    public Integer[][] sub(Integer[][] input1, Integer[][] input2) {
        checkArraySize(input1, input2);
        final Integer[][] result = new Integer[input1.length][input1[0].length];

        for (int i = 0; i < input1.length; ++i) {
            for (int j = 0; j < input1[0].length; ++j) {
                result[i][j] = input1[i][j] - input2[i][j];
            }
        }

        return result;
    }

    /**
     * Metoda dodaje komorki tablic i zwraca ich sume w trzeciej
     * tablicy
     *
     * @param input1 - 1 tablica wejsciowa
     * @param input2 - 2 tablica wejsciowa
     * @return tablica wynikowa
     */
    public Integer[][] add(Integer[][] input1, Integer[][] input2) {
        checkArraySize(input1, input2);
        final Integer[][] result = new Integer[input1.length][input1[0].length];

        for (int i = 0; i < input1.length; ++i) {
            for (int j = 0; j < input1[0].length; ++j) {
                result[i][j] = input1[i][j] + input2[i][j];
            }
        }

        return result;
    }

    private void checkArraySize(Integer[][] input1, Integer[][] input2) {
        assert input1.length == input2.length : "Main arrays size are diffrent";
    }


    public String fillToBlockSize(final String input) {
        LOGGER.info("Input data: {}", input.length());

        String result = input;
        int restElements;
        int amountElementsInBlock = BLOCK_SIZE * BLOCK_SIZE;

        if (input.length() > amountElementsInBlock) {
            restElements = amountElementsInBlock -
                    (input.length() % amountElementsInBlock);
        } else {
            restElements = amountElementsInBlock - input.length();
        }

        if (restElements == 0) {
            return result;
        }

        for (int i = 0; i < restElements; i++) {
            result += EMPTY;
        }
        LOGGER.info("Output data: {}", result.length());
        return result;
    }

    /**
     * Metoda dzieli podany tekxt na blocki o rozmiarze BLOCK_SIZE,
     * a nastepnie zwraca te bloki zaagregowane w tablicy.
     *
     * @param input Text ktory zostanie podzielony na bloki
     * @return Tablica trojwymiarowa zawierajaca bloki danych
     * o rozmiarze BLOCK_SIZE
     * @see #BLOCK_SIZE
     */
    public String[][][] splitToBlock(final String input) {
        //wyjsciowa tablica trojwymiarowa
        int blockAmount = input.length() / (BLOCK_SIZE * BLOCK_SIZE);
        if (blockAmount < 1) {
            blockAmount = 1;
        }

        String[][][] result =
                new String[blockAmount][ BLOCK_SIZE][BLOCK_SIZE];
        int currentPositionInInputString = 0;

        // Petla po blokach tekstu
        for (int i = 0; i < blockAmount; ++i) {
            // petla po kolumnach pojedynczego bloku
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                // petla po wierszach pojedynczego bloku
                for (int w = 0; w < BLOCK_SIZE; ++w) {
                    if (currentPositionInInputString == input.length()-1) {
                        result[i][k][w] = input.substring(currentPositionInInputString);
                    } else {
                        result[i][k][w] = input.substring(currentPositionInInputString,
                                currentPositionInInputString + 1);
                    }

                    currentPositionInInputString++;
                }
            }//koniec petli po kolumnach pojedynczego bloku
        }// koniec petli po blokach

        return result;
    }

    public Integer[][][] convertToAscii(String[][][] stringBlocks) {
        Integer[][][] result =
                new Integer[stringBlocks.length]
                        [stringBlocks[0].length]
                        [stringBlocks[0].length];

        // Petla po blokach tekstu
        for (int i = 0; i < stringBlocks.length; ++i) {
            result[i] = convertToAscii(stringBlocks[i]);

        }// koniec petli po blokach

        return result;
    }

    public Integer[][] convertToAscii(String[][] stringBlock) {
        Integer[][] result = new Integer[BLOCK_SIZE][BLOCK_SIZE];
        // petla po kolumnach pojedynczego bloku
        for (int k = 0; k < BLOCK_SIZE; ++k) {
            // petla po wierszach pojedynczego bloku
            for (int w = 0; w < BLOCK_SIZE; ++w) {
                result[k][w] = (int) (stringBlock[k][w]).charAt(0);
            }
        }//koniec petli po kolumnach pojedynczego bloku
        return result;
    }

    /**
     * Metoda zamienia text na blok znakow. W przypadku kiedy tekst jest za krotki
     * zostaje powielony tyle razy zeby zapelnic tablice. W przypadku kiedy jest
     * dluzszy niz rozmiar bloku - zostaje obciety.
     *
     * @param input tekst do zamiany na blok
     * @return blok danych o rozmiarze BLOCK_SIZE
     * @see #BLOCK_SIZE
     */
    public String[][] toSingleBlock(final String input) {
        assert input.length() > 0 : "Password cannot be null";

        String[][] result = new String[BLOCK_SIZE][BLOCK_SIZE];
        int positionInInput = 0;

        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                if (positionInInput >= input.length()) {
                    positionInInput = 0;
                }
                result[i][k] = String.valueOf(input.charAt(positionInInput));
                positionInInput++;
            }
        }

        return result;
    }

    public Integer[][] shift(Integer[][] input) {
        Integer[][] result = new Integer[input.length][input.length];
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                result[i][k] = input[k][i];
            }
        }
        return result;
    }

    public String convertFromBlock(Integer[][] input) {
        String result = "";
        for (int i = 0; i < BLOCK_SIZE; ++i) {
            for (int k = 0; k < BLOCK_SIZE; ++k) {
                result += String.valueOf((char) (int) input[i][k]);
            }
        }

        return result;
    }

}
