package com.sda.example;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Podstawowy kontroller dla widoku JavyFx.
 */
public class ApplicationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    @FXML
    private CheckBox isDecrypt;

    @FXML
    private TextArea inputData;

    @FXML
    private TextArea outputData;

    @FXML
    private TextField passwordField;

    @FXML
    private Button openFileButton;

    @FXML
    private Button runButton;

    /**
     * Service uzywany do zamiany tekstow.
     */
    private SimpleCryptoService simpleCryptoService = new SimpleCryptoService();


    public void onRun() {
        LOGGER.info("start");

        if (isIncorrectField(passwordField) || isIncorrectField(inputData)) {
            return;
        }

        final String password = passwordField.getText();
        final String inputText = inputData.getText();

        if (isDecrypt.isSelected()) {
            String result = simpleCryptoService.decrypt(inputText, password);
            outputData.setText(result);
        } else {
            String result = simpleCryptoService.encrypt(inputText, password);
            outputData.setText(result);
        }
    }

    public void onSelectFile(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        File onSelectFile = fileChooser.showOpenDialog(passwordField.getScene().getWindow());

        if (onSelectFile == null) {
            return;
        }

        try (BufferedReader br = new BufferedReader(new FileReader(onSelectFile))) {

            String line;
            String result = "";
            while ((line = br.readLine()) != null) {
                result += line + "\n";
            }

            inputData.setText(result);

        } catch (IOException e) {
            LOGGER.error("Some error: {}", e);
        }
    }

    /**
     * Walidacja czy pole zostalo prawidlowo ustawione.
     */
    public boolean isIncorrectField(final TextInputControl input) {
        String fieldValue = input.getText();
        if (fieldValue == null || fieldValue.equals("")) {
            LOGGER.warn("Nie podano hasla");
            setErrorStyleOnField(input);
            return true;
        } else {
            setCorrectStyleOnField(input);
            return false;
        }
    }

    private void setErrorStyleOnField(final TextInputControl input) {
        input.setStyle("-fx-border-width: 1px; -fx-border-color: red");
    }

    private void setCorrectStyleOnField(final TextInputControl input) {
        input.setStyle("-fx-border-width: 0px; -fx-border-color: black");
    }
}
