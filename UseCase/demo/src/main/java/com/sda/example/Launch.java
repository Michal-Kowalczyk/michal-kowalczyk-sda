package com.sda.example;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application Launcher.
 */
public class Launch extends Application {

    private final static Logger LOGGER = LoggerFactory.getLogger(Launch.class);

    public static void main(final String[] args) {
        LOGGER.info("Start application");
        Application.launch();
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("/szyfrator.fxml"));
        Scene scene = new Scene(root, 600, 400);

        primaryStage.setTitle("Szyfrator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
