package com.sda.example;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class SimpleCryptoService implements CryptoService {

    /**
     * Obiekt pomocniczy do wykonywania operacji na tablicach.
     */
    private TableService tableService = new TableService();

    private static final Logger LOGGER =
            LoggerFactory.getLogger(SimpleCryptoService.class);

    /**
     * Szyfrowanie.
     *
     * @param input tekst do zaszyfrowania
     * @param input haslo ktorym bedzie zaszyfrowane haslo
     * @return Zaszyfrowany tekst
     */
    @Override
    public String encrypt(final String input, final String password) {
        LOGGER.info("Input string: {}", input);

        final String filledText = tableService.fillToBlockSize(input);
        final String[][][] stringBlocks = tableService.splitToBlock(filledText);
        Arrays.stream(stringBlocks).forEach(t -> tableService.show(t));

        final Integer[][][] asciiTextBlock = tableService.convertToAscii(stringBlocks);
        Arrays.stream(asciiTextBlock).forEach(t -> tableService.show(t));

        final String[][] passwordBlock = tableService.toSingleBlock(password);
        tableService.show(passwordBlock);

        Integer[][] asciiPasswordBlock = tableService.convertToAscii(passwordBlock);
        tableService.show(asciiPasswordBlock);

        Integer[][][] codedResult = new Integer[asciiTextBlock.length]
                [asciiTextBlock[0].length]
                [asciiTextBlock[0].length];

        String encryptedText = "";
        for (int i = 0; i < asciiTextBlock.length; i++) {
            codedResult[i] = tableService.add(asciiTextBlock[i], asciiPasswordBlock);

            LOGGER.info("Dodane bloki");
            tableService.show(codedResult[i]);

            codedResult[i] = tableService.shift(codedResult[i]);
            LOGGER.info("Transponowane bloki");
            tableService.show(codedResult[i]);

            String blockAsString = tableService.convertFromBlock(codedResult[i]);
            LOGGER.info("blockAsString: {}", blockAsString);

            encryptedText += blockAsString;
        }

        return encryptedText;
    }

    /**
     * Deszyfracja.
     *
     * @param input
     * @return
     */
    @Override
    public String decrypt(final String input, final String password) {
        LOGGER.info("Input string: {}", input);

        final String[][] passwordBlock = tableService.toSingleBlock(password);
        Integer[][] asciiPasswordBlock = tableService.convertToAscii(passwordBlock);

        String result = "";
        final String[][][] stringBlocks = tableService.splitToBlock(input);
        for (int i = 0; i < stringBlocks.length; ++i) {
            Integer[][] intBlock = tableService.convertToAscii(stringBlocks[i]);
            intBlock = tableService.shift(intBlock);
            result += tableService.convertFromBlock(tableService.sub(intBlock, asciiPasswordBlock));
        }

        return result;
    }
}
