package com.sda.example;

public interface CryptoService {

    String encrypt(String input, String password);

    String decrypt(String input, String password);
}
