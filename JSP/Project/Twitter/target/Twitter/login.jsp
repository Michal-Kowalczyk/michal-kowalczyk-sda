<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>

</head>
<body>

	<%@ include file="/WEB-INF/jsp/common/navigation.jsp"%>

	<c:if test='${"ERROR" == requestScope.Status}'>
		<span class="label label-warning">Bad credentials</span>
	</c:if>

	<div class="container">
		<form class="form-signin" action="login" method="post">
			<h2 class="form-signin-heading">Please sign in</h2>

			<label for="login" class="sr-only">Login</label>
			<input type="text" id="login" name="login"
				class="form-control" placeholder="Login"
				required autofocus>

			<label for="inputPassword" class="sr-only">Password</label>
			<input type="password" id="inputPassword"
				name="inputPassword" class="form-control"
				placeholder="Password" required>

			<button class="btn btn-lg btn-primary btn-block"
			type="submit">Sign in</button>
		</form>
	</div>
	<!-- /container -->


	<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>
</body>
</html>