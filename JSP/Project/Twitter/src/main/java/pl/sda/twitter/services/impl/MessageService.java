package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import pl.sda.twitter.model.Message;
import pl.sda.twitter.services.IMessageService;


public class MessageService implements IMessageService{

	@Override
	public void insertMessage(String message) throws Exception {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);

			String INSERT_QUERY = "INSERT INTO messages (content) VALUES (?)";

			try(PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {
				preparedStatement.setString(1, message);

				preparedStatement.execute();
			}

			connection.commit();

		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			System.out.println(e.getMessage());
			throw new Exception("Blad zapisu wiadomosci");
//			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e){
			}
		}
	}

	@Override
	public List<Message> getMessages() {
		List<Message> messages = new ArrayList<>();
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);
			
			String GET_MESSAGE_QUERY = "SELECT id, content, create_date FROM messages";
			try(PreparedStatement preparedStatement = connection.prepareStatement(GET_MESSAGE_QUERY)) {
				ResultSet rs = preparedStatement.executeQuery();
				
				while(rs.next()) {
					Message message = new Message();
					message.setId(rs.getInt(1));
					message.setContent(rs.getString(2));
					message.setDate(rs.getString(3));
					messages.add(message);
				}
			}
			 
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e){
			}
		}	
		
		messages.sort(new Comparator<Message>() {

			@Override
			public int compare(Message o1, Message o2) {
				return o2.getDate().compareTo(o1.getDate());
			}

		});

		return messages;
	}

	@Override
	public void removeMessage(String id) {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"admin");
			connection.setAutoCommit(false);
			String DELETe_QUERY = "DELETE FROM messages WHERE id = ?";
			try (PreparedStatement preparedStatement = connection.prepareStatement(DELETe_QUERY)) {

				preparedStatement.setInt(1, Integer.valueOf(id));
		
				preparedStatement.execute();
			}
			connection.commit();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			System.out.println(e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
	}

}