package pl.sda.twitter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pl.sda.twitter.services.IMessageService;
import pl.sda.twitter.services.impl.MessageService;

public class MessagesListServlet extends HttpServlet {
	
	IMessageService messageService;
	{
		messageService = new MessageService();
	}


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		List<String> messagesFromSession = (List<String>) session.getAttribute("MessagesList");
		if (messagesFromSession == null) {
			messagesFromSession = new ArrayList<>();
		}

//		req.setAttribute("messagesList", messagesFromSession);

		IMessageService messageService = new MessageService();
		req.setAttribute("messagesList", messageService.getMessages());

		req.getRequestDispatcher("messagesList.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String messageId = req.getParameter("messageId");
	
		messageService.removeMessage(messageId);
	
		doGet(req, resp);
	}

}
