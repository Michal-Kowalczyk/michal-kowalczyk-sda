package pl.sda.twitter.services;

import java.util.List;

import pl.sda.twitter.model.Message;

public interface IMessageService {

	void insertMessage(String message) throws Exception;

	List<Message> getMessages();
	
	void removeMessage(String id);
}
