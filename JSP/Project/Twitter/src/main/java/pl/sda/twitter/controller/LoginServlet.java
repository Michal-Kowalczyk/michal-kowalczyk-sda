package pl.sda.twitter.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.sda.twitter.model.SessionData;
import pl.sda.twitter.services.impl.ILoginService;
import pl.sda.twitter.services.impl.LoginService;

public class LoginServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		if (!"".equals(req.getParameter("logout"))) {
			req.getSession().invalidate();
		}
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		
		
		String login = req.getParameter("login");
		String password = req.getParameter("inputPassword");
		
		ILoginService loginService = new LoginService();
	
		if(loginService.login(login, password)) {
			
			SessionData sessionData = new SessionData();
			sessionData.setLogin(login);
			req.getSession().setAttribute("SessionData", sessionData);
			
			resp.sendRedirect(req.getContextPath() + "/messagesList");
		}
		else {
			req.setAttribute("Status", "ERROR");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}
	}
}
