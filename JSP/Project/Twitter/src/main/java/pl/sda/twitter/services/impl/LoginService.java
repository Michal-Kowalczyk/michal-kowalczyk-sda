package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginService implements ILoginService  {

	@Override
	public boolean login(String login, String password) {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			connection.setAutoCommit(false);

			String GET_LOGIN_QUERY = "SELECT * FROM \"twitterUsers\" " + "WHERE login = ? AND password = ?";

			try (PreparedStatement preparedStatement = connection.prepareStatement(GET_LOGIN_QUERY)) {

				preparedStatement.setString(1, login);
				preparedStatement.setString(2, password);
				
				ResultSet rs = preparedStatement.executeQuery();

				if (rs.next()) {
					return true;
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e){
			}
		}
		return false;
	}

	public void logout() {
	}
}
