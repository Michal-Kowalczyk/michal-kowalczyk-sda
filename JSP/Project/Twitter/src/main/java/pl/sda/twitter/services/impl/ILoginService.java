package pl.sda.twitter.services.impl;

public interface ILoginService {

	boolean login(String login, String password);

	void logout();


}
