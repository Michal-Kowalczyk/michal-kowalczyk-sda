package pl.sda.twitter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pl.sda.twitter.services.IMessageService;
import pl.sda.twitter.services.impl.MessageService;

public class AddMessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("addMessage.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String message = req.getParameter("message");
		System.out.println("Tresc wiadomosci: " + message);

		HttpSession session = req.getSession();
		List<String> messagesFromSession = (List<String>) session.getAttribute("MessagesList");
		if (messagesFromSession == null) {
			messagesFromSession = new ArrayList<>();
		}

		messagesFromSession.add(message);

		session.setAttribute("MessagesList", messagesFromSession);

		IMessageService messageService = new MessageService();
		try {
			messageService.insertMessage(message);
			req.setAttribute("result", "SUCCESS");
		} catch (Exception e) {
			req.setAttribute("result", "ERROR");
		}
		req.getRequestDispatcher("addMessage.jsp").forward(req, resp);
	}
}
