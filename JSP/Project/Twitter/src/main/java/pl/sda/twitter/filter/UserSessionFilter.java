package pl.sda.twitter.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserSessionFilter implements Filter {
	
	private String excludeUrl;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		
		boolean isExclude = httpRequest.getServletPath().startsWith(excludeUrl);
		
		if (!isExclude && httpRequest.getSession().getAttribute("SessionData") == null) {
			httpResponse.sendRedirect(httpRequest.getContextPath() + "/login");
		}
		else {
			chain.doFilter(httpRequest, httpResponse);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		excludeUrl = filterConfig.getInitParameter("EXCLUDE_URL");
	}

}
