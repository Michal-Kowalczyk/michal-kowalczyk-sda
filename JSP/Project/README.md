<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


Twitter - prosty serwis internetowy umożliwiający dodawanie krótkich wiadomości tekstowych
1.	Zastosowanie wzorca MVC. Kontrolery w postaci serwletów, widoki w postaci JSP oraz model danych w postaci JavaBeans
2.	Połączenie do bazy danych
3.	Autoryzacja do aplikacji
4.	Wykorzystanie mechanizmu sesji do przechowywania danych o użytkowniku
5.	Wykorzystanie mechanizmu cookies do zapamiętywania ustawień użytkownika
6.	Wykorzystanie Maven
7.	Do layoutu można zastosować bibliotekę bootstrap
8.	Filtr logujący do konsoli nagłówki żądania
9.	Filtr sprawdzający czy użytkownik jest zalogowany do aplikacji


Do poprawnego działania programu wymagane jest połączenie z serwerem oraz bazą danych (w celu ustalenia hasła i loginu do twittera).