package pl.sda.zoo.animal;

public interface Animal {

    String saySomething();

    String say(String word);

    void run();

    void stop();

}
