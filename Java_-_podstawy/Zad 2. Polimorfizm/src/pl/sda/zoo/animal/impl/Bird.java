package pl.sda.zoo.animal.impl;

import pl.sda.zoo.animal.Animal;
import pl.sda.zoo.animal.AnimalTemplate;

public class Bird extends AnimalTemplate {

    public Bird(String name) {
        super(name);
    }

    @Override
    public String saySomething() {
        return "Cwir-cwir";
    }

    @Override
    public String say(String word) {
        return "Bird says " + word;
    }

    @Override
    public void run() {
        System.out.println("Bird starts running");
    }

    @Override
    public void stop() {
        System.out.println("Bird stops");
    }
}
