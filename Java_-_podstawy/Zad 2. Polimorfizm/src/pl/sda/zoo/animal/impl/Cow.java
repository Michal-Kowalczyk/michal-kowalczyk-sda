package pl.sda.zoo.animal.impl;

import pl.sda.zoo.animal.AnimalTemplate;

public class Cow extends AnimalTemplate {

    public Cow(String name) {
        super(name);
    }

    @Override
    public String saySomething() {
        return "Mu";
    }

    @Override
    public String say(String word) {
        return "Cow says " + word;
    }

    @Override
    public void run() {
        System.out.println("Cow starts running");
    }

    @Override
    public void stop() {
        System.out.println("Cow stops");
    }
}

