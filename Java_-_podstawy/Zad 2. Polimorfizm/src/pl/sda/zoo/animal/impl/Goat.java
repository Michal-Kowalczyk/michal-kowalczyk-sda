package pl.sda.zoo.animal.impl;

import pl.sda.zoo.animal.Animal;
import pl.sda.zoo.animal.AnimalTemplate;

public class Goat extends AnimalTemplate {
    public Goat (String name) {
        super(name);
    }

    @Override
    public String saySomething() {
        return "Me";
    }

    @Override
    public String say(String word) {
        return "Goat says " + word;
    }

    @Override
    public void run() {
        System.out.println("Goat starts running");
    }

    @Override
    public void stop() {
        System.out.println("Goat stops");
    }
}
