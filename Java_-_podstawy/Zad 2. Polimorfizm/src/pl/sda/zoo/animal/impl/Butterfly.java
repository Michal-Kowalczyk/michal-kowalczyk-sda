package pl.sda.zoo.animal.impl;

import pl.sda.zoo.animal.Animal;
import pl.sda.zoo.animal.AnimalTemplate;

public class Butterfly extends AnimalTemplate {

    public Butterfly(String name) {
        super(name);
    }

    @Override
    public String saySomething() {
        return "Hi";
    }

    @Override
    public String say(String word) {
        return "Butterfly says " + word;
    }

    @Override
    public void run() {
        System.out.println("Butterfly running");
    }

    @Override
    public void stop() {
        System.out.println("Butterfly stops");
    }
}
