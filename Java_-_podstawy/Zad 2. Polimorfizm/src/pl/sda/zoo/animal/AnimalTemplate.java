package pl.sda.zoo.animal;

public abstract class AnimalTemplate implements Animal {

    private final String name;

    public AnimalTemplate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
