import pl.sda.zoo.animal.Animal;
import pl.sda.zoo.animal.impl.Butterfly;
import pl.sda.zoo.animal.impl.Cow;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Cow krowka = new Cow("Mucka");
        System.out.println(krowka.saySomething());

        Butterfly motyl = new Butterfly("Motylek");
        System.out.println(motyl.say("Hi"));

        Animal animal;
        animal = new Cow("Mucka 2");
        animal.saySomething();

        List<Animal> animals = new ArrayList<>();
        animals.add(krowka);
        animals.add(motyl);
        animals.add(animal);

        System.out.println("All animals say");
        for (Animal currentAnimal : animals) {
            System.out.println(
                    currentAnimal.saySomething());
        }

    }

        private static String getClassName (String fullName) {
            return fullName.substring(fullName.lastIndexOf(".") + 1, fullName.length());

    }
}
