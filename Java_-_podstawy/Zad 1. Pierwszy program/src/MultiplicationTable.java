/**
 * Created by MICHAL on 2016-08-02.
 */

//g. Tworzymy klasę MultiplicationTable. Następnie dodajemy metodę show, która w parametrze
// przyjmuje jedną liczbę całkowitą i nie zwraca wyniku. Wynikiem działania metody ma być
// wypisanie w konsoli tabliczki mnożenia dla zakresu 1 - 'liczba wprowadzona w parametrze'


public class MultiplicationTable {

    public static void main(String[] args) {
        //show(7);
        int tableSize = 4;
        int[][] table = get(tableSize);

        //Wypisanie z użyciem for-each
        for (int[] row : table) {
            for (int val : row) {
                System.out.print(val + " ");
            }
            System.out.println();
        }

        //Wypisanie z użyciem klasycznego for
        for (int i = 0; i < tableSize; i++) {
            for (int j = 0; j < tableSize; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }


    public static void show(int number) {
        for (int i = 1; i <= number; i++) {
            for (int j = 1; j <= number; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }

    public static int[][] get(int number) {
        int[][] result = new int[number][number];
        for (int i = 0; i < number; i++) {
            for (int j = 0; j < number; j++) {
                result[i][j] = (i + 1) * (j + 1);
            }
        }
        return result;
    }
}
