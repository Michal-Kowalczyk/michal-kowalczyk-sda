/**
 * Created by MICHAL on 2016-08-02.
 */

//f. Zmieniamy nazwę klasy z IfElseCondition na ValueFinder.
//Przerabiamy metodę findMin tak, aby zamiast 3 parametrów typu int przyjmowała tablicę
//elementów typu int; przerabiamy kod funkcji pod obsługę nowego parametru.


public class ValueFinder {
    public static void main(String[] args) {
        int[] otherNumbers = new int[]{1,2,3,4};
        System.out.println(findMin(otherNumbers));
    }

    public static int findMin(int[] numbers){
        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            if (currentNumber < min) {
                min = currentNumber;
            }
        }
        return min;
    }
}