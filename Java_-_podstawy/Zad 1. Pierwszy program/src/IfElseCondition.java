/**
 * Created by MICHAL on 2016-08-02.
 */

//a. Tworzymy klasę IfElseCondition
//b. Dopisujemy metodę main
//c. Tworzymy statyczną metodę findMin(int a, int b, int c), która zwraca najmniejszy z parametrów
//d. Wywołujemy findMin z metody main
//e. Analogicznie tworzymy metodę findMed(int a, int b, int c), która zwraca wartość środkową



public class IfElseCondition {

    public static void main(String[] args) {
        int min = findMin(3, 1, 4);
        System.out.println("Wartośc minimalna dla liczb 3,1,4 to: " + min);
        int med = findMed(5, 3, 7);
        System.out.println("Wartość środkowa dla liczb 5,3,7 to: " + med);
    }

    public static int findMin(int a, int b, int c) {
        int min = a;
        if (b < min) {
            min = b;
        }
        if (c < min) {
            min = c;
        }
        return min;
    }

    public static int findMed(int a, int b, int c) {
        if (a > b && a > c) {
            return b > c ? b : c;
        } else if (b > a && b > c) {
            return a > c ? a : c;
        } else {
            return a > b ? a : b;
        }
    }
}
