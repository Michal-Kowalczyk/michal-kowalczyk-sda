package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class Main extends Application {

    private Box box = new Box(300, 300, 300);

    private PerspectiveCamera camera = new PerspectiveCamera(false);


    @Override
    public void start(Stage stage) {
        // Create a Box
        box.setTranslateX(400);
        box.setTranslateY(300);
        box.setTranslateZ(200);

        PhongMaterial mat = new PhongMaterial();
        Image diffuseMap = new Image(this.getClass().getResourceAsStream("./wood.jpg"));
        mat.setDiffuseMap(diffuseMap);
        mat.setSpecularColor(Color.WHITE);
        box.setMaterial(mat);

        // Create a Light
        PointLight light = new PointLight();
        light.setTranslateX(550);
        light.setTranslateY(300);
        light.setTranslateZ(0);
        light.setColor(Color.SANDYBROWN);
        light.setBlendMode(BlendMode.MULTIPLY);

        PointLight light2 = new PointLight();
        light2.setTranslateX(50);
        light2.setTranslateY(1000);
        light2.setTranslateZ(0);
        light2.setColor(Color.DARKRED);
        light2.setBlendMode(BlendMode.MULTIPLY);

        // Create a Camera to view the 3D Shapes
        camera.setTranslateX(100);
        camera.setTranslateY(-50);
        camera.setTranslateZ(300);

        // Add the Shapes and the Light to the Group
        Group root = new Group(box, light,light2);

        // Create a Scene with depth buffer enabled
        Scene scene = new Scene(root, 800, 900, true, SceneAntialiasing.BALANCED);


        EventHandler<KeyEvent> eventHandler = event -> rotateBox(event);

        scene.setOnKeyPressed(eventHandler);
        // Add the Camera to the Scene
        scene.setCamera(camera);
        scene.setFill(Color.SIENNA);
        stage.setScene(scene);
        stage.setTitle("An Example with Predefined 3D Shapes");
        stage.show();
    }

    private void rotateBox(KeyEvent event) {

        if (event.getCode()== KeyCode.LEFT) {

        } else if(event.getCode()==KeyCode.RIGHT) {

        }

        box.setRotationAxis(Rotate.Y_AXIS);
        box.setRotate(box.getRotate() + 1);

        final Translate t = new Translate(0.0, 0.0, 0.0);
        final Rotate rx = new Rotate(box.getRotationAxis().getX() + 1, 0, 0, 0, Rotate.X_AXIS);
        final Rotate ry = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
        final Rotate rz = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);

        box.getTransforms().addAll(t, rx, ry, rz);
    }


    public static void main(String[] args) {
        launch(args);
    }
}