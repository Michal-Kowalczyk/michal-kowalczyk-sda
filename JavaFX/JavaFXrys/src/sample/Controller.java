package sample;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.QuadCurve;


public class Controller {

    private Ellipse ellipse2 = new Ellipse(10,18);
    private Circle circlel = new Circle(10);
    private boolean isBlink;
    private int counter;



    @FXML
    private Group groupForDraw;
    @FXML
    public void initialize() {
        System.out.println("Start");
        Circle circle = new Circle(100);
        circle.setFill(Color.WHEAT);
        circle.setCenterX(150);
        circle.setCenterY(150);
        circle.setStroke(Color.TAN);
        circle.setStrokeWidth(10);
        groupForDraw.getChildren().add(circle);

        Ellipse ellipse1 = new Ellipse(10,18);
        ellipse1.setFill(Color.BEIGE);
        ellipse1.setCenterX(120);
        ellipse1.setCenterY(100);
        groupForDraw.getChildren().add(ellipse1);

        ellipse2.setFill(Color.BEIGE);
        ellipse2.setCenterX(180);
        ellipse2.setCenterY(100);
        groupForDraw.getChildren().add(ellipse2);

        Circle circlep = new Circle(10);
        circlep.setFill(Color.BLACK);
        circlep.setCenterX(120);
        circlep.setCenterY(100);
        groupForDraw.getChildren().add(circlep);

        circlel.setFill(Color.BLACK);
        circlel.setCenterX(180);
        circlel.setCenterY(100);
        groupForDraw.getChildren().add(circlel);


        QuadCurve quad = new QuadCurve();
        quad.setStartX(120);
        quad.setStartY(180);
        quad.setEndX(180);
        quad.setEndY(180);
        quad.setControlX(150);
        quad.setControlY(200);
        quad.setFill(Color.RED);
        groupForDraw.getChildren().add(quad);

        Polygon polygon = new Polygon();
        polygon.getPoints().addAll(new Double[]{
                140.0, 150.0,
                150.0, 170.0,
                160.0, 150.0 });
        groupForDraw.getChildren().add(polygon);

        AnimationTimer ourTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                counter ++;
                if (counter == 60) {

                    blinkBkink();
                    counter = 0;
                }
            }
        };
        ourTimer.start();
        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.SANDYBROWN);
        dropShadow.setOffsetX(20);
        dropShadow.setOffsetY(20);

        GaussianBlur gaussianBlur = new GaussianBlur();
        gaussianBlur.setRadius(15);
        gaussianBlur.setInput(dropShadow);

        Reflection reflection = new Reflection();
        reflection.setInput(dropShadow);
        reflection.setFraction(0.8);

        circle.setEffect(dropShadow);

    }

    private void blinkBkink() {
        if (isBlink) {
            ellipse2.setFill(Color.BLACK);
            circlel.setFill(Color.WHITE);
            isBlink = false;
        }  else {
            ellipse2.setFill(Color.BEIGE);
            circlel.setFill(Color.BLACK);
            isBlink = true;
        }
    }


}
