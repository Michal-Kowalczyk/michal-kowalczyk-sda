package sample.content;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;

import java.util.LinkedList;

public class TopContent implements IContentCreator {

        HBox hbox = new HBox();


    @Override
    public void createContent() {
        Label label = new Label("Select");
        ComboBox<String> comboBox = new ComboBox<>();

        LinkedList<String> comboText = new LinkedList<>();
        comboText.add("option 1");
        comboText.add("option 2");
        comboText.add("option 3");

        ObservableList<String> comboBoxElement = FXCollections.observableArrayList(comboText);
        comboBox.setItems(comboBoxElement);

        hbox.getChildren().add(label);
        hbox.getChildren().add(comboBox);

    }

    @Override
    public WebEngine attachToLayout(BorderPane paneToAttach) {
        paneToAttach.setTop(hbox);

        return null;
    }
}
