package sample.content;

import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.web.WebEngine;

public class BottomContent implements IContentCreator {
       public WebEngine webEngine;
        FlowPane flowPane = new FlowPane();

    @Override
    public void createContent() {
        DatePicker datePicker = new DatePicker();
        Slider slider = new Slider();
        Hyperlink hyperlink = new Hyperlink("Hyperlink1");
        Button button4 = new Button("Button 4");
        Button button5 = new Button("Button 5");
        Button button6 = new Button("Button 6");
        hyperlink.setOnAction(e -> webEngine.load("http://www.wp.pl"));
        flowPane.getChildren().addAll(datePicker, slider, button4, button5, button6, hyperlink);

    }

    @Override
    public WebEngine attachToLayout(BorderPane paneToAttach) {
        paneToAttach.setBottom(flowPane);

        return null;
    }
}
