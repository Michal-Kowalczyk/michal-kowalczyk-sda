package sample.content;

import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;

public interface IContentCreator {

    void createContent();

    WebEngine attachToLayout(BorderPane paneToAttach);
}
