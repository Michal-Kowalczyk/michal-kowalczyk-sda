package sample.content;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class CenterContent implements IContentCreator {
        WebEngine webEngine;
        TabPane tabPane = new TabPane();
        WebView webView = new WebView();
    @Override
    public void createContent() {
        WebEngine webEngine = webView.getEngine();
        webEngine.load("http://www.onet.pl");
        tabPane.getTabs().add(new Tab("1", webView));

    }

    @Override
    public WebEngine attachToLayout(BorderPane paneToAttach) {
        paneToAttach.setCenter(tabPane);
        return webEngine;

    }
}
