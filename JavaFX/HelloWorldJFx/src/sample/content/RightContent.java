package sample.content;

import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;

public class RightContent implements IContentCreator {

        Accordion accordion = new Accordion();

    @Override
    public void createContent() {
        TitledPane t1 = new TitledPane("T1", new Button("Button1"));
        TitledPane t2 = new TitledPane("T2", new Button("Button2"));
        TitledPane t3 = new TitledPane("T3", new Button("Button3"));
        accordion.getPanes().addAll(t1, t2, t3);

    }

    @Override
    public WebEngine attachToLayout(BorderPane paneToAttach) {

        paneToAttach.setRight(accordion);
        return null;
    }
}
