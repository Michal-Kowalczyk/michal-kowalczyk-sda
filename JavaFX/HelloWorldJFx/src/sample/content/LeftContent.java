package sample.content;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;

public class LeftContent implements IContentCreator {

        VBox vBox = new VBox();

    @Override
    public void createContent() {
        Button button1 = new Button("Button 1");
        Button button2 = new Button("Button 2");
        Button button3 = new Button("Button 3");
        vBox.getChildren().add(button1);
        vBox.getChildren().add(button2);
        vBox.getChildren().add(button3);
    }

    @Override
    public WebEngine attachToLayout(BorderPane paneToAttach) {
        paneToAttach.setLeft(vBox);

        return null;
    }
}
