package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

import javafx.scene.web.WebEngine;
import javafx.stage.Stage;
import sample.content.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        BorderPane borderPane = new BorderPane();

        LeftContent leftContent = new LeftContent();
        leftContent.createContent();
        leftContent.attachToLayout(borderPane);

        RightContent rightContent = new RightContent();
        rightContent.createContent();
        rightContent.attachToLayout(borderPane);

        TopContent topContent = new TopContent();
        topContent.createContent();
        topContent.attachToLayout(borderPane);

        BottomContent bottomContent = new BottomContent();
        bottomContent.createContent();
        bottomContent.attachToLayout(borderPane);

        CenterContent centerContent = new CenterContent();
        centerContent.createContent();
        centerContent.attachToLayout(borderPane);





        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(borderPane, 300, 275));
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
