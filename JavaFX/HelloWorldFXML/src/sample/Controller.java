package sample;


import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.util.ArrayList;
import java.util.List;


public class Controller {

    @FXML
    WebView webView;

    WebEngine webEngine;

    @FXML
    Hyperlink hyperlink;

    public void runHyper(ActionEvent actionEvent) {
        webView.getEngine().load("http://www.wp.pl");

    }
    @FXML
    Accordion mySuperAccordion;

    public void buttoncli(ActionEvent actionEvent) {
        mySuperAccordion.getPanes().add(new TitledPane("1", new Label("dddd")));
    }
    @FXML
    public void initialize() {
        webEngine = webView.getEngine();
        webEngine.load("http://www.onet.pl");

    }
}